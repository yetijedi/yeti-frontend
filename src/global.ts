import { onErrorHandler } from "./app/services/errorLogger";

/**
 * @override
 */
const registerEvents = () => {
    window.onerror = onErrorHandler;
};

export default registerEvents;
