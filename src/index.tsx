import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import configureStore, { getHistory } from "./app/store/configureStore";

import 'antd/dist/antd.css';
import './global.scss';

import ErrorBoundary from "./app/ErrorBoundary";
import registerEvents from "./global";
import AppRoutes from "./app/AppRoutes";

// Register Global Events and properties
registerEvents();

ReactDOM.render(
    <ErrorBoundary>
        <Provider store={configureStore()}>
            <AppRoutes history={getHistory()}/>
        </Provider>
    </ErrorBoundary>,
    document.getElementById('root')
);


