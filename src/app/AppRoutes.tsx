import React, { lazy, Suspense } from 'react'

import { BrowserRouter } from 'react-router-dom'
import { ConnectedRouter } from 'connected-react-router'
import PrivateRoute from './PrivateRoute'
import App from './App'
import { Route, RouteProps, Switch } from 'react-router'
import NotFoundPage from './views/layouts/NotFoundPage'
import Loader from './views/components/UI/loading/Loading'

import { RESOURCE_CODES, METHODS } from './constants'

const RootLayout = lazy(() => import('./views/layouts/RootLayout'))
const UsersList = lazy(() => import('./views/pages/users'))
const CreateUpdateUser = lazy(() => import('./views/pages/users/CreateUpdateUser'))
const ProductsList = lazy(() => import('./views/pages/products'))
const CreateUpdateProduct = lazy(() => import('./views/pages/products/CreateUpdateProduct'))
const CategoriesList = lazy(() => import('./views/pages/categories'))
const CreateUpdateCategory = lazy(() => import('./views/pages/categories/CreateUpdateCategory'))
const OrdersList = lazy(() => import('./views/pages/orders'))
const UpdateOrder = lazy(() => import('./views/pages/orders/UpdateOrder'))
const ShopsList = lazy(() => import('./views/pages/shops'))
const CreateUpdateShop = lazy(() => import('./views/pages/shops/CreateUpdateShop'))
const SalesPlanList = lazy(() => import('./views/pages/sales-plan/SalesPlanList'))
const Login = lazy(() => import('./views/layouts/login/Login'))

const {
    users: USER,
    shops: SHOP,
    products: PRODUCT,
    orders: ORDERS,
    categories: CATEGORY,
    sales_plan: SALES_PLAN,
} = RESOURCE_CODES

const {
    get: GET,
    post: POST,
    put: PUT,
} = METHODS

const publicRoutes: RouteProps[] = [
    {
        path: '/',
        component: RootLayout,
        exact: true,
    },
    {
        path: '/login',
        component: Login,
    },
]

const privateRoutes: any[] = [
    {
        path: '/users',
        component: UsersList,
        resource: USER,
        method: GET,
        exact: true,
    },
    {
        path: '/users/update/:id',
        component: CreateUpdateUser,
        resource: USER,
        method: PUT,
        exact: true,
    },
    {
        path: '/users/create',
        component: CreateUpdateUser,
        resource: USER,
        method: POST,
        exact: true,
    },
    {
        path: '/products',
        component: ProductsList,
        resource: PRODUCT,
        method: GET,
        exact: true,
    },
    {
        path: '/products/create',
        component: CreateUpdateProduct,
        resource: PRODUCT,
        method: POST,
        exact: true,
    },
    {
        path: '/products/update/:id',
        component: CreateUpdateProduct,
        resource: PRODUCT,
        method: PUT,
        exact: true,
    },
    {
        path: '/categories',
        component: CategoriesList,
        resource: CATEGORY,
        method: GET,
        exact: true,
    },
    {
        path: '/categories/create',
        component: CreateUpdateCategory,
        resource: CATEGORY,
        method: POST,
        exact: true,
    },
    {
        path: '/categories/update/:id',
        component: CreateUpdateCategory,
        resource: CATEGORY,
        method: PUT,
        exact: true,
    },
    {
        path: '/orders',
        component: OrdersList,
        resource: ORDERS,
        method: GET,
        exact: true,
    },
    {
        path: '/orders/update/:id',
        component: UpdateOrder,
        resource: ORDERS,
        method: PUT,
        exact: true,
    },
    {
        path: '/shops',
        component: ShopsList,
        resource: SHOP,
        method: GET,
        exact: true,
    },
    {
        path: '/shops/create',
        component: CreateUpdateShop,
        resource: SHOP,
        method: POST,
        exact: true,
    },
    {
        path: '/shops/update/:id',
        component: CreateUpdateShop,
        resource: SHOP,
        method: PUT,
        exact: true,
    },
    {
        path: '/sales-plan',
        component: SalesPlanList,
        resource: SALES_PLAN,
        method: GET,
    },
    // {
    //     path: '/expenses',
    //     component: Expenses,
    //     resource: 'EXPENSE',
    // },
]

class AppRoutes extends React.Component<any> {
    render() {
        const { history } = this.props
        return (
            <BrowserRouter>
                <ConnectedRouter history={history}>
                    <App>
                        <Suspense fallback={<Loader fullScreen={true}/>}>
                            <Switch>
                                {privateRoutes.map(route => (
                                    <PrivateRoute
                                        method={route.method}
                                        resource={route.resource}
                                        path={route.path}
                                        component={route.component}
                                        key={route.path as string}
                                        exact={route.exact}
                                    />
                                ))}
                                {publicRoutes.map(route => (
                                    <Route
                                        path={route.path}
                                        component={route.component}
                                        key={route.path as string}
                                        exact={route.exact}
                                    />
                                ))}
                                <Route component={NotFoundPage}/>
                            </Switch>
                        </Suspense>
                    </App>
                </ConnectedRouter>
            </BrowserRouter>
        )
    }
}

export default AppRoutes
