import NProgress from 'nprogress'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router-dom'
import { Redirect } from 'react-router'
import { globalOp, globalSel } from './store/ducks/global'
import LoginLayout from './views/layouts/login/Login'
import MainLayout from './views/layouts/main/Main'

const Layouts = {
    login: LoginLayout,
    main: MainLayout,
}

let previousPath = ''

function App({ children }) {
    const dispatch = useDispatch()
    const location = useLocation()
    const isAuthenticated = useSelector(globalSel.isAuthSelector)
    const { pathname, search } = location;

    useEffect(() => {
        if (isAuthenticated) {
            dispatch(globalOp.getCommonSettingsAsync())
        }
    }, [isAuthenticated])

    useEffect(() => {
        if (pathname !== previousPath) {
            previousPath = pathname
            window.scrollTo(0, 0)
        }
    }, [pathname])

    // NProgress Management
    const currentPath = pathname + search
    if (currentPath !== previousPath) {
        NProgress.start()
    }

    setTimeout(() => {
        NProgress.done()
        previousPath = currentPath
    }, 300)

    // Layout Rendering
    const getLayout = () => {
        if (pathname === '/login') {
            return 'login'
        }
        return 'main'
    }

    const Container = Layouts[getLayout()]
    const isLoginLayout = getLayout() === 'login'

    const BootstrappedLayout = () => {
        // redirect to login page if current is not login page and user not authorized
        if (!isLoginLayout && !isAuthenticated) {
            return <Redirect to="/login"/>
        }
        // redirect to main dashboard when user on login page and authorized
        if (isLoginLayout && isAuthenticated) {
            return <Redirect to="/"/>
        }

        // in other case render previously set layout
        // @ts-ignore
        return <Container>{children}</Container>
    }

    return BootstrappedLayout()
}

export default App
