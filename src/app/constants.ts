export const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT
export const DEFAULT_LANG = 'en'
export const DEFAULT_CURRENCY = '֏'

export const getEnvironment = () => process.env.NODE_ENV
export const isDevelopment = process.env.NODE_ENV !== 'production'

export const RESOURCE_CODES = {
    users: 'USER',
    shops: 'SHOP',
    products: 'PRODUCT',
    orders: 'ORDERS',
    categories: 'CATEGORY',
    sales_plan: 'SALES_PLAN',
}

export const METHODS = {
    get: 'GET',
    post: 'POST',
    put: 'PUT',
    delete: 'DELETE',
}

export const SIDEBAR_ITEMS = [
    {
        title: 'Dashboard',
        key: 'dashboard',
        url: '/',
        icon: 'icmn-home',
    },
    {
        title: 'Shops',
        key: 'shops',
        url: '/shops',
        icon: 'icmn-library',
    },
    {
        title: 'Orders',
        key: 'orders',
        url: '/orders',
        icon: 'icmn-list',
    },
    {
        title: 'Products',
        key: 'products',
        url: '/products',
        icon: 'icmn-spoon-knife',
    },
    {
        title: 'Categories',
        key: 'categories',
        url: '/categories',
        icon: 'icmn-stack',
    },
    {
        title: 'Sales Plan',
        key: 'sales-plan',
        url: '/sales-plan',
        icon: 'icmn-stats-bars2',
    },
    // {
    //     title: 'Workspace',
    //     key: 'workspace',
    //     url: '/workspace',
    //     icon: 'icmn-laptop'
    // },
].map(item => ({ ...item, resourceCode: RESOURCE_CODES[item.key] || '' }))


