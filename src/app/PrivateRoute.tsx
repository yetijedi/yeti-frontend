import React, { FC } from 'react'
import { useSelector } from 'react-redux'
import { Route } from 'react-router'
import { globalSel } from './store/ducks/global'
import { checkUserAccess } from './services/checkUserAccess'
import UnauthorizedAccess from './views/layouts/UnauthorizedAccessPage'

interface IProps {
    component: any;
    resource: string;
    method: string;
    path: string;
    key: string;
    exact: boolean;
}

const PrivateRoute: FC<IProps> = (
    {
        component: Component,
        resource,
        method,
        ...rest
    },
) => {
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const hasAccess = checkUserAccess(currentUser, commonSettings, resource, method)

    return (
        <Route
            {...rest}
            render={
                props => {
                    return hasAccess
                        ? <Component {...props} />
                        : !commonSettings || !currentUser
                            ? null
                            : <UnauthorizedAccess/>
                }
            }
        />
    )
}

export default PrivateRoute
