import { connectRouter } from 'connected-react-router'
import salesPlan from "./ducks/sales-plan";
import { combineReducers } from "@reduxjs/toolkit";
import { workspaceSlice } from "./ducks/workspace/workspaceSlice";
import { productsSlice } from "./ducks/products/productsSlice";
import { categoriesSlice } from './ducks/categories/categoriesSlice'
import { shopsSlice } from './ducks/shops/shopsSlice'
import { globalSlice } from './ducks/global/globalSlice'
import { usersSlice } from './ducks/users/usersSlice'
import { ordersSlice } from './ducks/orders/ordersSlice'


export default (history) => combineReducers({
    router: connectRouter(history),
    salesPlan,
    orders: ordersSlice.reducer,
    users: usersSlice.reducer,
    global: globalSlice.reducer,
    shops: shopsSlice.reducer,
    categories: categoriesSlice.reducer,
    products: productsSlice.reducer,
    workspace: workspaceSlice.reducer
});
