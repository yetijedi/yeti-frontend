import { IOrdersState } from "./typings";

export function initialOrdersState(): IOrdersState {
    return {
        isLoading: false,
        error: {},
        orders: [],
    }
}
