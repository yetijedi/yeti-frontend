import { createSelector } from 'reselect'

const orderSelector = state => state.orders

const loadingSelector = createSelector(
    [orderSelector], orders => orders.isLoading,
)

export default {
    orderSelector,
    loadingSelector,
}
