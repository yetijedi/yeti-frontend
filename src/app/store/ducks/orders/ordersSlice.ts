import { createSlice } from '@reduxjs/toolkit'
import { initialOrdersState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const ordersSlice = createSlice({
    name: 'orders',
    initialState: initialOrdersState(),
    reducers: {
        fetchOrders(state) {
            state.orders = []
        },
        fetchOrdersSuccess(state, action) {
            state.orders = action.payload.map(o => ({ ...o, key: o.id }))
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
    },
})
