import { ordersSlice } from './ordersSlice'

export { default as ordersOp } from './operations'
export { default as ordersSel } from './selectors'
export default ordersSlice
