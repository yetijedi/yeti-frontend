import fetch from '../../../services/fetch'
import { batch } from 'react-redux'
import { ordersSlice } from './ordersSlice'

const {
    setError,
    setLoading,
    fetchOrders,
    fetchOrdersSuccess,
} = ordersSlice.actions

const getOrdersAsync = () => {
    return async (dispatch) => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchOrders())
        })
        try {
            const { data } = await fetch({
                endPoint: '/orders',
                method: 'GET',
            })
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchOrdersSuccess(data))
            })
        } catch (e) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(e.response.data))
            })
        }
    }
}

/**
 *  =======================================
 *            GET SINGLE ORDERS
 *  =======================================
 *  @url /orders/{id}
 *  @method GET
 *  @param orderId {number}
 */
const getSingleOrderAsync = orderId => {
    return fetch({
        endPoint: `/orders/${orderId}`,
        method: 'GET',
    })
}

/**
 *  =======================================
 *              UPDATE ORDERS
 *  =======================================
 *  @url /orders/{id}
 *  @method PUT
 *  @param params {object}
 */
const updateOrderAsync = params => {
    return fetch({
        endPoint: `/orders/${params.id}`,
        method: 'PUT',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              DELETE ORDER
 *  =======================================
 *  @url /orders/{id}
 *  @method DELETE
 *  @param orderId {number}
 */
const deleteOrderAsync = orderId => {
    return fetch({
        endPoint: `/orders/${orderId}`,
        method: 'DELETE',
    })
}

export default {
    getOrdersAsync,
    getSingleOrderAsync,
    updateOrderAsync,
    deleteOrderAsync,
}
