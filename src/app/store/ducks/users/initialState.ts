import { IUsersState } from "./typings";

export function initialUsersState(): IUsersState {
    return {
        isLoading: false,
        error: {},
        users: []
    }
}
