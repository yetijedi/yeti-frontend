import { createSlice } from '@reduxjs/toolkit'
import { initialUsersState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const usersSlice = createSlice({
    name: 'users',
    initialState: initialUsersState(),
    reducers: {
        fetchUsers(state) {
            state.users = []
        },
        fetchUsersSuccess(state, action) {
            state.users = action.payload.map(u => ({ ...u, key: u.id }))
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
    },
})
