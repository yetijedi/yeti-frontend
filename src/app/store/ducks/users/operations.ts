import fetch from '../../../services/fetch';
import { usersSlice } from './usersSlice'
import { batch } from 'react-redux'

const {
    setError,
    setLoading,
    fetchUsers,
    fetchUsersSuccess
} = usersSlice.actions

/**
 *  =======================================
 *                 GET USERS
 *  =======================================
 *  @url /users/
 *  @method GET
 */
const getUsersAsync = () => {
    return async (dispatch) => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchUsers())
        })
        try {
            const { data } = await fetch({
                endPoint: '/users',
                method: 'GET'
            });
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchUsersSuccess(data))
            })
        } catch (err) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(err.response.data))
            })
        }
    }
};

/**
 *  =======================================
 *            GET SINGLE USER
 *  =======================================
 *  @url /users/{id}
 *  @method GET
 *  @param userId {number}
 */
const getSingleUserAsync = userId => {
    return fetch({
        endPoint: `/users/${userId}`,
        method: 'GET',
    })
}

/**
 *  =======================================
 *               UPDATE USER
 *  =======================================
 *  @url /users/{id}
 *  @method PUT
 *  @param params {object}
 */
const updateUserAsync = params => {
    return fetch({
        endPoint: `/users/${params.id}`,
        method: 'PUT',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *                CREATE USER
 *  =======================================
 *  @url /users
 *  @method POST
 *  @param params {object}
 */
const createUserAsync = params => {
    return fetch({
        endPoint: `/users`,
        method: 'POST',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *               DELETE USER
 *  =======================================
 *  @url /users/{id}
 *  @method DELETE
 *  @param userId {number}
 */
const deleteUserAsync = userId => {
    return fetch({
        endPoint: `/users/${userId}`,
        method: 'DELETE',
    })
}

export default {
    getUsersAsync,
    getSingleUserAsync,
    createUserAsync,
    updateUserAsync,
    deleteUserAsync
};
