import { createSelector } from "reselect";
const usersSelector = state => state.users;

const loadingSelector = createSelector(
    [usersSelector], users => users.loading
);

export default {
    usersSelector,
    loadingSelector,
}
