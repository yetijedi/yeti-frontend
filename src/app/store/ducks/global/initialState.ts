import { IGlobalState } from './typings'
import { getLocale } from './thunks/locale.thunk'
import { extractCurrentUser, isAuthenticated } from './thunks/auth.thunk'

// function initialCurrentUserRoleState(): ICurrentUserRoleState {
//     return {
//         id: -1,
//         name: '',
//         dashboardType: -1,
//     }
// }

// function initialCurrentUserPermissionState(): ICurrentUserPermissionState {
//     return {
//         ORD: false,
//         PROD: false,
//         USR: false,
//         SHOP: false,
//         CAT: false,
//         SAL: false,
//         EXP: false,
//     }
// }

// function initialCurrentUserState(): ICurrentUserState {
//     return {
//         id: -1,
//         email: '',
//         shop_id: -1,
//         name: '',
//         phone: '',
//         role: {},
//         permissions: {},
//     }
// }

export function initialGlobalState(): IGlobalState {
    return {
        error: {},
        locale: getLocale(),
        authenticated: isAuthenticated(),
        currentUser: extractCurrentUser(true) || null,
        commonSettings: null,
    }
}
