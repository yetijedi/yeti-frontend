export interface ICurrentUserRoleState {
    id: number;
    dashboardType: number;
    name: string;
}

export interface ICurrentUserPermissionState {
    ORD: boolean;
    PROD: boolean;
    USR: boolean;
    SHOP: boolean;
    CAT: boolean;
    SAL: boolean;
    EXP: boolean;
}

export interface ICurrentUserState {
    id: number;
    email: string;
    shop_id: number;
    name: string;
    phone: string;
    role: any;
    permissions: any;
}

export interface IGlobalState {
    error: any,
    locale: string;
    authenticated: boolean;
    currentUser: ICurrentUserState;
    commonSettings: any
}


