import { loginAsync, logoutAsync, forgotPasswordAsync } from './thunks/auth.thunk'
import { changeLocale } from './thunks/locale.thunk'
import fetch from '../../../services/fetch'
import { globalSlice } from './globalSlice'

/**
 *  =======================================
 *             COMMON SETTING
 *  =======================================
 *  @url /common/settings
 *  @method GET
 */
const getCommonSettingsAsync = () => {
    const { setCommonSettings, setError } = globalSlice.actions
    return async (dispatch) => {
        dispatch(setError({}))
        try {
            const { data } = await fetch({
                endPoint: '/common/settings',
                method: 'GET',
            })
            dispatch(setCommonSettings(data))
        } catch (e) {
            dispatch(setError(e.response.data))
        }
    }
}

export default {
    changeLocale,
    loginAsync,
    logoutAsync,
    forgotPasswordAsync,
    getCommonSettingsAsync
}
