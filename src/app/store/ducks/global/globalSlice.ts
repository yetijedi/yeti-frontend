import { createSlice } from '@reduxjs/toolkit'
import { initialGlobalState } from './initialState'

export const globalSlice = createSlice({
    name: 'global',
    initialState: initialGlobalState(),
    reducers: {
        setCurrentUser(state, action) {
            state.currentUser = action.payload
        },
        loginSuccess(state) {
            state.authenticated = true
        },
        logOutUser(state) {
            state.authenticated = false
            state.currentUser = null
            state.commonSettings = null
        },
        setLocale(state, action) {
            state.locale = action.payload.locale
        },
        setCommonSettings(state, action) {
            state.commonSettings = action.payload
        },
        setError(state, action) {
            state.error = action.payload
        },
    },
})
