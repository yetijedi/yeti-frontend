import { createSelector } from 'reselect'
import { isAuthenticated } from './thunks/auth.thunk'

const globalSelector = state => state.global
const isAuthSelector = createSelector(
    [globalSelector],
    global => global.authenticated || isAuthenticated(),
)

const currentUserSelector = createSelector(
    [globalSelector],
    global => global.currentUser,
)

const commonSettingsSelector = createSelector(
    [globalSelector],
    global => global.commonSettings,
)

export default {
    globalSelector,
    currentUserSelector,
    commonSettingsSelector,
    isAuthSelector,
}
