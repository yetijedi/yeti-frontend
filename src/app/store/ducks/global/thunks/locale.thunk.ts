import Store from 'store'
import { globalSlice } from '../globalSlice'
import { DEFAULT_LANG } from '../../../../constants'

export const getLocale = () => Store.get('locale') || DEFAULT_LANG

export const changeLocale = (locale) => {
    const { setLocale } = globalSlice.actions

    return dispatch => {
        Store.set('locale', locale)
        dispatch(setLocale(locale))
    }
}
