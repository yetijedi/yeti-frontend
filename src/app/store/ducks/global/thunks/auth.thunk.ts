import jwt_decode from 'jwt-decode'
import CookieStore from 'store/storages/cookieStorage'
import fetch from '../../../../services/fetch'
import { ICurrentUserState } from '../typings'
import { globalSlice } from '../globalSlice'

/**
 * @desc entry point
 */
export const loginAsync = (phone: string, password: string, rememberMe: boolean) => {
    const {
        setCurrentUser,
        loginSuccess,
    } = globalSlice.actions

    return dispatch => {
        if (!phone || !password) {
            throw new Error(`Please type correct username and password`)
        }

        const isAuth = isAuthenticated()
        if (isAuth) {
            return dispatch(logoutAsync())
        }
        fetch({
            endPoint: '/auth/login',
            body: {
                phone,
                password,
                rememberMe,
            },
            method: 'POST',
        }).then(() => {
            dispatch(loginSuccess())
            try {
                const extractedUser = extractCurrentUser()
                dispatch(setCurrentUser(extractedUser))
            } catch {
                dispatch(logoutAsync())
            }
        })
    }

}

export const logoutAsync = () => {
    const {
        logOutUser,
    } = globalSlice.actions

    return dispatch => {
        fetch({
            endPoint: '/auth/logout',
            method: 'POST',
        }).then(() => {
            dispatch(logOutUser())
        })
    }
}

export const forgotPasswordAsync = (username: string) => {
    const { logOutUser } = globalSlice.actions
    return dispatch => {
        fetch({
            endPoint: '/password/reset',
            method: 'POST',
        }).then(() => {
            dispatch(logOutUser())
        })
    }
}

export const extractCurrentUser = (initial: boolean = false): ICurrentUserState => {
    const authInfo = getAuthCookie()
    if (authInfo) {
        return jwt_decode(authInfo) as ICurrentUserState
    } else if (!authInfo && !initial) {
        throw new Error('Payload cookie not found')
    }
}

export const getAuthCookie = () => CookieStore.read('_payload') || ''

export const isAuthenticated = () => !!getAuthCookie() || false


