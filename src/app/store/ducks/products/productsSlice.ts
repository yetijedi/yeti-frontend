import { createSlice } from '@reduxjs/toolkit'
import { initialProductsState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const productsSlice = createSlice({
    name: 'products',
    initialState: initialProductsState(),
    reducers: {
        fetchProducts(state) {
            state.products = [];
        },
        fetchProductsSuccess(state, action) {
            state.products = action.payload.map(p => ({ ...p, key: p.id }));
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
    },
})
