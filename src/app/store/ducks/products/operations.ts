import { batch } from 'react-redux'
import { productsSlice } from './productsSlice'
import fetch from '../../../services/fetch'

const {
    setError,
    setLoading,
    fetchProducts,
    fetchProductsSuccess,
} = productsSlice.actions

/**
 *  =======================================
 *              GET PRODUCTS
 *  =======================================
 *  @url /products
 *  @method GET
 */
const getProductsAsync = () => {
    return async (dispatch) => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchProducts())
        })
        try {
            const { data } = await fetch({
                endPoint: '/products',
                method: 'GET',
            })
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchProductsSuccess(data))
            })
        } catch (e) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(e.response.data))
            })
        }
    }
}

/**
 *  =======================================
 *            GET SINGLE PRODUCT
 *  =======================================
 *  @url /products/{id}
 *  @method GET
 *  @param productId {number}
 */
const getSingleProductAsync = productId => {
    return fetch({
        endPoint: `/products/${productId}`,
        method: 'GET',
    })
}

/**
 *  =======================================
 *              UPDATE PRODUCT
 *  =======================================
 *  @url /products/{id}
 *  @method PUT
 *  @param params {object}
 */
const updateProductAsync = params => {
    return fetch({
        endPoint: `/products/${params.id}`,
        method: 'PUT',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              CREATE PRODUCT
 *  =======================================
 *  @url /products
 *  @method POST
 *  @param params {object}
 */
const createProductAsync = params => {
    return fetch({
        endPoint: `/products`,
        method: 'POST',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              DELETE PRODUCT
 *  =======================================
 *  @url /products/{id}
 *  @method DELETE
 *  @param productId {number}
 */
const deleteProductAsync = productId => {
    return fetch({
        endPoint: `/products/${productId}`,
        method: 'DELETE',
    })
}

export default {
    getProductsAsync,
    getSingleProductAsync,
    updateProductAsync,
    createProductAsync,
    deleteProductAsync,
}
