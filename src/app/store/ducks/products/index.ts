import { productsSlice } from "./productsSlice";

export { default as productsOp } from './operations';
export { default as productsSel } from './selectors';

export default productsSlice;
