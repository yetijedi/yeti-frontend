import { IProductsState } from "./typings";

export function initialProductsState(): IProductsState {
    return {
        isLoading: false,
        error: {},
        products: [],
    }
}
