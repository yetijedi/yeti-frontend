import { createSelector } from 'reselect'

const productSelector = state => state.products

const loadingSelector = createSelector(
    [productSelector],
    state => state.isLoading,
)

export default {
    productSelector,
    loadingSelector,
}
