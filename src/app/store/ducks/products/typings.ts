export interface IProductsState {
    isLoading: boolean;
    error: any;
    products: [],
}
