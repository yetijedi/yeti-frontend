import { createSelector } from "reselect";
const salesPlanSelector = state => state.salesPlan;

const loadingSelector = createSelector(
    [salesPlanSelector], salesPlan => salesPlan.loading
);

export default {
    salesPlanSelector,
    loadingSelector,
}
