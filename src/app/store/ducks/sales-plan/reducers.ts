import * as types from "./types";

const salesPlan = (state = [], action: any = {}) => {
    switch (action.type) {
        case (types.FETCH_QUERIES_REQUEST):
            return {
                ...state,
                loading: true
            };

        case (types.FETCH_QUERIES_SUCCESS):
            return {
                ...state,
                rows: action.payload.map(s => ({ ...s, key: s.id })),
                loading: false,
            };

        case (types.FETCH_QUERIES_FAILURE):
            return {
                ...state,
                loading: false,
            };

        default:
            return state;
    }
};

export default salesPlan;
