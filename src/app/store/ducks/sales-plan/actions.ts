import * as types from "./types";

export const fetchQueries = () => ({
    type: types.FETCH_QUERIES_REQUEST
});

export const fetchQueriesSuccess = salesPlan => ({
    type: types.FETCH_QUERIES_SUCCESS,
    payload: [
        ...salesPlan,
    ]
});

export const fetchQueriesFailure = error => ({
    type: types.FETCH_QUERIES_FAILURE,
    payload: {
        error
    }
});
