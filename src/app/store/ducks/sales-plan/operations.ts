import { fetchQueries, fetchQueriesFailure, fetchQueriesSuccess } from "./actions";
import fetch from '../../../services/fetch';

const getSalesPlanAsync = () => {
    return async (dispatch) => {
        dispatch(fetchQueries());
        try {
            const { data } = await fetch({
                endPoint: '/sales-plan',
                method: 'GET'
            });
            dispatch(fetchQueriesSuccess(data));
        } catch (err) {
            dispatch(fetchQueriesFailure(err.response.data));
        }
    }
};

export default {
    getSalesPlanAsync,
};
