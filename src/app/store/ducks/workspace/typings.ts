export interface IWorkspaceState {
    isLoading: boolean;
    error: any;
    basket: any[],
    data: {
        products: [];
        categories: [];
        shop: any;
        lastOrderN: number;
    }
}

export interface IBasket {

}

export interface IBasketItem {

}
