import { createSlice } from '@reduxjs/toolkit'
import { initialWorkspaceState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const workspaceSlice = createSlice({
    name: 'workspace',
    initialState: initialWorkspaceState(),
    reducers: {
        fetchWorkspace(state) {
            state.error = {}
            state.basket = []
            state.data = {
                products: [],
                categories: [],
                lastOrderN: 0,
                shop: {},
            }
        },
        fetchWorkspaceSuccess(state, action) {
            const { products, categories, last_order_id, shop } = action.payload
            state.data = {
                products,
                categories,
                lastOrderN: last_order_id,
                shop,
            }
        },
        fetchOrderCreateSuccess(state) {
            state.basket = []
            state.data.lastOrderN++
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
        setBasket(state, action) {
            const { id, quantity, sell_price } = action.payload

            const existItemIndex = state.basket.findIndex(item => item.id === id)
            const existItem = state.basket[existItemIndex]

            if (quantity === 0) {
                state.basket.splice(existItemIndex, 1)
            } else {
                if (existItem) {
                    if (typeof quantity === 'undefined') {
                        existItem.quantity++
                    } else {
                        existItem.quantity = quantity
                    }

                    existItem.itemTotalPrice = existItem.quantity * sell_price

                } else {
                    state.basket.push({ ...action.payload, quantity: 1, itemTotalPrice: sell_price })
                }
            }
        },
        setBasketForUpdate(state, action) {
            state.basket = action.payload
        },
    },
})
