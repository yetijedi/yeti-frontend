import { batch } from 'react-redux'
import { workspaceSlice } from './workspaceSlice'
import fetch from '../../../services/fetch'
import { notify } from '../../../services/notifier'

const {
    setError,
    setLoading,
    fetchWorkspace,
    fetchWorkspaceSuccess,
    fetchOrderCreateSuccess,
    setBasket,
    setBasketForUpdate,
} = workspaceSlice.actions

/**
 * Fetch types modifier
 */
const fetchWorkspaceAsync = () => {
    return async dispatch => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchWorkspace())
        })
        try {
            const { data } = await fetch({
                endPoint: '/workspace',
            })
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchWorkspaceSuccess(data))
            })
        } catch (e) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(e.response.data))
            })
        }
    }
}

/**
 * =========================================
 *               CREATE ORDER
 * =========================================
 * @url /orders
 * @method POST
 * @param orderParams {map}
 */
const fetchCreateOrderAsync = orderParams => {
    return async dispatch => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
        })
        try {
            await fetch({
                endPoint: '/orders',
                method: 'POST',
                body: JSON.stringify(orderParams),
            })
            notify('Order created successfully!')
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchOrderCreateSuccess())
            })
        } catch (e) {
            dispatch(setLoading(false))
            dispatch(setError(e.response.data))
            notify('Order create failed!', 'error');
        }
    }
}

export default {
    fetchWorkspaceAsync,
    fetchCreateOrderAsync,
    setBasket,
    setBasketForUpdate,
}
