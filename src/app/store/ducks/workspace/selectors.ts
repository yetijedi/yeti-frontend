import { createSelector } from 'reselect'

const workspaceSelector = state => state.workspace

const isLoadingSelector = createSelector(
    [workspaceSelector],
    state => state.isLoading,
)

const errorSelector = createSelector(
    [workspaceSelector],
    state => state.error,
)

const dataSelector = createSelector(
    [workspaceSelector],
    state => state.data,
)

const basketSelector = createSelector(
    [workspaceSelector],
    state => state.basket,
)

const basketTotalPriceSelector = createSelector(
    [workspaceSelector],
    state => state.basket.reduce((acc, cur) => acc + cur.itemTotalPrice, 0),
)

export default {
    workspaceSelector,
    isLoadingSelector,
    errorSelector,
    dataSelector,
    basketSelector,
    basketTotalPriceSelector
}
