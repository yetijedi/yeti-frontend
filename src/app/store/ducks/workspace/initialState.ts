import { IWorkspaceState } from "./typings";

export function initialWorkspaceState(): IWorkspaceState {
    return {
        isLoading: false,
        error: {},
        basket: [],
        data: {
            products: [],
            categories: [],
            shop: {},
            lastOrderN: 0,
        }
    }
}
