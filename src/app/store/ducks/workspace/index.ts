import { workspaceSlice } from "./workspaceSlice";

export { default as workspaceOp } from './operations';
export { default as workspaceSel } from './selectors';

export default workspaceSlice;
