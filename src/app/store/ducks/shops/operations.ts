import { batch } from 'react-redux'
import fetch from '../../../services/fetch';
import { shopsSlice } from './shopsSlice'

const {
    setError,
    setLoading,
    fetchShops,
    fetchShopsSuccess,
} = shopsSlice.actions

const getShopsAsync = () => {
    return async (dispatch) => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchShops())
        })
        try {
            const { data } = await fetch({
                endPoint: '/shops',
                method: 'GET'
            });
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchShopsSuccess(data))
            })
        } catch (e) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(e.response.data))
            })
        }
    }
};

/**
 *  =======================================
 *            GET SINGLE SHOP
 *  =======================================
 *  @url /shops/{id}
 *  @method GET
 *  @param shopId {number}
 */
const getSingleShopAsync = shopId => {
    return fetch({
        endPoint: `/shops/${shopId}`,
        method: 'GET',
    })
}

/**
 *  =======================================
 *              UPDATE SHOP
 *  =======================================
 *  @url /shops/{id}
 *  @method PUT
 *  @param params {object}
 */
const updateShopAsync = params => {
    return fetch({
        endPoint: `/shops/${params.id}`,
        method: 'PUT',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              CREATE SHOP
 *  =======================================
 *  @url /shops
 *  @method POST
 *  @param params {object}
 */
const createShopAsync = params => {
    return fetch({
        endPoint: `/shops`,
        method: 'POST',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              DELETE SHOP
 *  =======================================
 *  @url /shops/{id}
 *  @method DELETE
 *  @param shopId {number}
 */
const deleteShopAsync = shopId => {
    return fetch({
        endPoint: `/shops/${shopId}`,
        method: 'DELETE',
    })
}

export default {
    getShopsAsync,
    getSingleShopAsync,
    updateShopAsync,
    createShopAsync,
    deleteShopAsync
};
