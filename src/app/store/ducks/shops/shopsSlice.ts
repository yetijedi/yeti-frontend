import { createSlice } from '@reduxjs/toolkit'
import { initialShopsState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const shopsSlice = createSlice({
    name: 'shops',
    initialState: initialShopsState(),
    reducers: {
        fetchShops(state) {
            state.shops = []
        },
        fetchShopsSuccess(state, action) {
            state.shops = action.payload.map(s => ({ ...s, key: s.id }))
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
    },
})
