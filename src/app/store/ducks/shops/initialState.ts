import { IShopsState } from "./typings";

export function initialShopsState(): IShopsState {
    return {
        isLoading: false,
        error: {},
        shops: []
    }
}
