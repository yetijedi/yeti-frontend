import { createSelector } from 'reselect'

const shopSelector = state => state.shops

const loadingSelector = createSelector(
    [shopSelector],
    shops => shops.isLoading,
)

export default {
    shopSelector,
    loadingSelector,
}
