import { shopsSlice } from "./shopsSlice";

export { default as shopsOp } from './operations';
export { default as shopsSel } from './selectors';
export default shopsSlice;
