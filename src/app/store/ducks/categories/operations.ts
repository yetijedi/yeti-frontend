import { batch } from 'react-redux'
import fetch from '../../../services/fetch'
import { categoriesSlice } from './categoriesSlice'

const {
    setError,
    setLoading,
    fetchCategories,
    fetchCategoriesSuccess,
} = categoriesSlice.actions

const getCategoriesAsync = () => {
    return async (dispatch) => {
        batch(() => {
            dispatch(setLoading(true))
            dispatch(setError({}))
            dispatch(fetchCategories())
        })
        try {
            const { data } = await fetch({
                endPoint: '/categories',
                method: 'GET',
            })
            batch(() => {
                dispatch(setLoading(false))
                dispatch(fetchCategoriesSuccess(data))
            })
        } catch (e) {
            batch(() => {
                dispatch(setLoading(false))
                dispatch(setError(e.response.data))
            })
        }
    }
}

/**
 *  =======================================
 *            GET SINGLE CATEGORY
 *  =======================================
 *  @url /categories/{id}
 *  @method GET
 *  @param categoryId {number}
 */
const getSingleCategoryAsync = categoryId => {
    return fetch({
        endPoint: `/categories/${categoryId}`,
        method: 'GET',
    })
}

/**
 *  =======================================
 *              UPDATE CATEGORY
 *  =======================================
 *  @url /categories/{id}
 *  @method PUT
 *  @param params {object}
 */
const updateCategoryAsync = params => {
    return fetch({
        endPoint: `/categories/${params.id}`,
        method: 'PUT',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              CREATE CATEGORY
 *  =======================================
 *  @url /categories
 *  @method POST
 *  @param params {object}
 */
const createCategoryAsync = params => {
    return fetch({
        endPoint: `/categories`,
        method: 'POST',
        body: JSON.stringify(params),
    })
}

/**
 *  =======================================
 *              DELETE CATEGORY
 *  =======================================
 *  @url /categories/{id}
 *  @method DELETE
 *  @param categoryId {number}
 */
const deleteCategoryAsync = categoryId => {
    return fetch({
        endPoint: `/categories/${categoryId}`,
        method: 'DELETE',
    })
}

export default {
    getCategoriesAsync,
    getSingleCategoryAsync,
    updateCategoryAsync,
    createCategoryAsync,
    deleteCategoryAsync
}
