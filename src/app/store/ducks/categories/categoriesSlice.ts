import { createSlice } from '@reduxjs/toolkit'
import { initialCategoriesState } from './initialState'
import { generateNetworkError, isNetworkError } from '../../../services/errorLogger'

export const categoriesSlice = createSlice({
    name: 'categories',
    initialState: initialCategoriesState(),
    reducers: {
        fetchCategories(state) {
            state.categories = []
        },
        fetchCategoriesSuccess(state, action) {
            state.categories = action.payload.map(c => ({ ...c, key: c.id }))
        },
        setError(state, action) {
            let error = action.payload
            if (isNetworkError(error)) {
                error = generateNetworkError(error)
            }
            state.error = error
        },
        setLoading(state, action) {
            state.isLoading = action.payload
        },
    },
})
