import { createSelector } from 'reselect'

const categorySelector = state => state.categories

const loadingSelector = createSelector(
    [categorySelector],
    categories => categories.isLoading,
)

export default {
    categorySelector,
    loadingSelector,
}
