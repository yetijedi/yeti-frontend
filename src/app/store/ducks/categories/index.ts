import { categoriesSlice } from "./categoriesSlice";

export { default as categoriesOp } from './operations';
export { default as categoriesSel } from './selectors';
export default categoriesSlice;
