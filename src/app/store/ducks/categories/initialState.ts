import { ICategoriesState } from "./typings";

export function initialCategoriesState(): ICategoriesState {
    return {
        isLoading: false,
        error: {},
        categories: []
    }
}
