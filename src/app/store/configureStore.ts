import rootReducer from './reducers';
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { createBrowserHistory } from 'history';
import { routerMiddleware } from "connected-react-router";
import { createLogger } from 'redux-logger'
import { isDevelopment } from "../constants";

let store;
const history = createBrowserHistory();

export const getStore = () => store;
export const getHistory = () => history;

export default function (options: any = {}) {
    const enhancers = [];
    const middleware = [
        ...getDefaultMiddleware({
            serializableCheck: false,
        }),
        routerMiddleware(getHistory()),
    ];
    if (isDevelopment) {
        const logger = createLogger({
            collapsed: true
        });
        middleware.push(logger);
    }

    store = configureStore({
        reducer: rootReducer(getHistory()),
        middleware,
        devTools: isDevelopment,
        enhancers
    });

    return store;
}

