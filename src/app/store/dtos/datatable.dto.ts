import { IOrderBy } from "./action.dto";

export class PaginationDto {
    limit: number = 10;
    offset: number = 0;
    page: number = 1;
    totalCount: number = 0;
}

export class DatatableDto {
    rows: any[] = [];
    columns: any[] = [];
    loading: boolean = false;
    pagination: PaginationDto = new PaginationDto();
    sortedBy: IOrderBy[] = [];
    filters: any = {};
    searchString: string = '';
}
