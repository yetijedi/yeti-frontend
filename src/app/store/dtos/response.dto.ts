export class SparqlResponseHead {
    vars?: string[] = [];
}

export class SparqlResponseResults {
    bindings?: any[] = [];
}

export class SparqlErrorResponse {
    status: number;
    title: string;
    detail: string;
    constructor(status, title, detail) {
        this.status = status;
        this.title = title;
        this.detail = detail;
    }
}

export class SparqlResponse {
    head: SparqlResponseHead = new SparqlResponseHead();
    results: SparqlResponseResults = new SparqlResponseResults();
    totalCount: number;
}
