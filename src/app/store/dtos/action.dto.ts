export interface IOrderBy {
    column: string;
    modifier: string;
}

export interface IActionMeta {
    method?: string;
    headers?: any;
    payload: any;
}

export interface IReduxAction {
    type: string;
    meta: IActionMeta;
}
