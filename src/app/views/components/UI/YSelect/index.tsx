import React, { memo } from 'react'
import { Select } from 'antd'

const { Option } = Select

const YSelect = (
    {
        options = [],
        showSearch = true,
        defaultOption = false,
        defaultValue = '',
        placeholder = '',
    },
) => {
    return (
        <Select
            showSearch={showSearch}
            placeholder={placeholder}
        >
            {defaultOption && <Option value={defaultValue}>{placeholder}</Option>}
            {
                options.map(s => (
                    <Option key={s.id} value={s.id}>{s.name}</Option>
                ))
            }
        </Select>
    )
}

export default memo(YSelect)
