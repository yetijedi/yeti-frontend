import React, { memo } from 'react'
import { thousandsFormatter } from '../../../../services/formatters'
import { DEFAULT_CURRENCY } from '../../../../constants'

const TotalAmount = ({ totalAmount, right = '' }) => {
    return (
        <>
            <div className={`${right} mb-3 font-weight-bold font-size-14 text-uppercase`}>Total</div>
            <div
                className={`${right} font-size-18 font-weight-bold`}>
                {`${thousandsFormatter(totalAmount)} ${DEFAULT_CURRENCY}`}
            </div>
        </>
    )
}

export default memo(TotalAmount)
