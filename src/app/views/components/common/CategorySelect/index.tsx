import React, { FC, memo, useEffect } from 'react'
import { Form, Select } from 'antd'
import { useDispatch, useSelector } from 'react-redux'
import { categoriesOp, categoriesSel } from '../../../../store/ducks/categories'
import { globalSel } from '../../../../store/ducks/global'

const { Option } = Select
const FormItem = Form.Item

interface IProps {
    withForm?: boolean;
    rules?: any;
    label?: string;
    showSearch?: boolean;
    handleOnChange?: (value: string) => void;
}

const CategorySelect: FC<IProps> = (
    {
        // categories,
        withForm = true,
        rules = [],
        label = 'Category',
        showSearch = true,
        handleOnChange = null,
    },
) => {

    const dispatch = useDispatch()
    const { categories } = useSelector(categoriesSel.categorySelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)

    useEffect(() => {
        dispatch(categoriesOp.getCategoriesAsync())
    }, [])

    return (
        <>
            {withForm
                ? (
                    <FormItem
                        rules={rules}
                        name="category_id"
                        label={label}>
                        <Select
                            showSearch={showSearch}
                            placeholder="Select a category"
                        >
                            <Option value="">Select a category</Option>
                            {
                                categories.map(s => (
                                    <Option key={s.id} value={s.id}>
                                        {commonSettings.defaultCategoryId === s.id ? 'Unknown Category' : s.name}
                                    </Option>
                                ))
                            }
                        </Select>
                    </FormItem>
                )
                : (
                    <Select
                        showSearch={showSearch}
                        onChange={handleOnChange}
                        placeholder="Select category"
                    >
                        {
                            categories.map(s => (
                                <Option key={s.id} value={s.id}>
                                    {commonSettings.defaultCategoryId === s.id ? 'Unknown Category' : s.name}
                                </Option>
                            ))
                        }
                    </Select>
                )}
        </>
    )
}

export default memo(CategorySelect)
