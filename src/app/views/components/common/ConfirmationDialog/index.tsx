import React from 'react'
import { Modal } from 'antd'
import { WarningOutlined } from '@ant-design/icons'

/**
 * @param title {string}
 * @param content {Element | string}
 * @param onConfirm {function}
 * @param isUpdate {boolean}
 */
export function confirmationDialog(onConfirm, content, isUpdate = false, title = 'Attention') {
    Modal.confirm({
        title,
        icon: <WarningOutlined/>,
        content: isUpdate ? content : `Are you sure you want to delete ${content}?`,
        onOk: onConfirm,
        okText: 'Confirm',
        okType: 'danger',
        cancelText: 'Cancel',
    })
}
