import React, { memo, useEffect } from 'react'
import { Form, Select } from 'antd'
import { shopsOp, shopsSel } from '../../../../store/ducks/shops'
import { useDispatch, useSelector } from 'react-redux'
import { globalSel } from '../../../../store/ducks/global'

const { Option } = Select
const FormItem = Form.Item

const ShopSelect = (
    {
        rules = [{ required: true }],
        showSearch = true,
        label = 'Shop',
    },
) => {
    const dispatch = useDispatch()
    const { shops } = useSelector(shopsSel.shopSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)

    useEffect(() => {
        dispatch(shopsOp.getShopsAsync())
    }, [])

    return (
        <FormItem
            rules={rules}
            name="shop_id"
            label={label}>
            <Select
                showSearch={showSearch}
                placeholder="Select a shop"
            >
                {
                    shops.map(s => (
                        <Option key={s.id} value={s.id}>
                            {commonSettings.defaultShopId === s.id ? 'Unknown Shop' : s.name}
                        </Option>
                    ))
                }
            </Select>
        </FormItem>
    )
}

export default memo(ShopSelect)
