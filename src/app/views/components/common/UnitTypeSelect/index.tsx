import React, { memo } from 'react'
import { Form, Select } from 'antd'

const { Option } = Select
const FormItem = Form.Item

// TEMPORARY
const unitTypes = [
    { id: 1, name: 'kg' },
    { id: 2, name: 'g' },
    { id: 3, name: 'lt' },
    { id: 4, name: 'mlt' },
]

const UnitTypeSelect = (
    // If needed make changes, make more dynamic
    {
        rules = [{ required: true, message: 'Unit type is required' }],
        label = 'Unit Type',
        showSearch = true,
    },
) => {
    return (
        <FormItem
            rules={rules}
            name="unit_type_id"
            label={label}>
            <Select
                showSearch={showSearch}
                placeholder="Select a unit type"
            >
                {
                    unitTypes.map(s => (
                        <Option key={s.id} value={s.id}>{s.name}</Option>
                    ))
                }
            </Select>
        </FormItem>
    )
}

export default memo(UnitTypeSelect)
