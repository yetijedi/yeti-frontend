import React, { memo } from 'react'
import { Button, Form, Input } from 'antd'
import { Link } from 'react-router-dom'
import styles from './style.module.scss'
import { globalOp } from '../../../store/ducks/global'
import { useDispatch } from 'react-redux'

const Forgot = () => {
    const [form] = Form.useForm()
    const dispatch = useDispatch()

    function handleSubmit(e) {
        e.preventDefault()
        dispatch(globalOp.forgotPasswordAsync(form.getFieldValue('username')))
    }

    return (
        <div>
            <div className={styles.block}>
                <div className="row">
                    <div className="col-xl-12">
                        <div className={styles.inner}>
                            <div className={styles.form}>
                                <h4 className="text-uppercase">
                                    <strong>Restore Password</strong>
                                </h4>
                                <br/>
                                <Form form={form} layout="vertical" hideRequiredMark>
                                    <Form.Item
                                        name="username"
                                        rules={[{ required: true, message: 'Please input username or email' }]}
                                        label="Username">
                                        <Input size="middle"/>
                                    </Form.Item>
                                    <div className="mb-2">
                                        <Link to="/login" className="utils__link--blue utils__link--underlined">
                                            Back to login
                                        </Link>
                                    </div>
                                    <div className="form-actions">
                                        <Link to="/">
                                            <Button
                                                type="primary"
                                                className="width-150 mr-4"
                                                htmlType="submit"
                                                loading={false}
                                                onClick={handleSubmit}
                                            >
                                                Restore Password
                                            </Button>
                                        </Link>
                                    </div>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default memo(Forgot)
