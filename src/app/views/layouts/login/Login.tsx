import { Button, Checkbox, Form, Input } from 'antd'
import { LockOutlined, UserOutlined } from '@ant-design/icons'

import React, { memo } from 'react'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { globalOp } from '../../../store/ducks/global'
import styles from './style.module.scss'

function LoginLayout() {
    const [form] = Form.useForm()
    const FormItem = Form.Item
    const dispatch = useDispatch()

    const handleSubmit = async e => {
        e.preventDefault()
        try {
            const validFields = await form.validateFields()
            dispatch(globalOp.loginAsync(
                validFields['username'],
                validFields['password'],
                validFields['remember']),
            )
        } catch (e) {

        }
    }

    return (
        <div>
            <section
                className={`${styles.login} ${styles.fullscreen}`}
                style={{ backgroundImage: `url('resources/images/photos/2.jpeg')` }}
            >
                <div className={styles.content}>
                    <div className={styles.form}>
                        <p className={styles.formTitle}>Welcome to Yeti Dashboard</p>

                        <Form className="login-form" form={form} name={'loginForm'}>
                            <FormItem
                                label="Username"
                                name={'username'}
                                rules={[{ required: true, message: 'Please input your username!' }]}
                            >
                                <Input
                                    prefix={<UserOutlined style={{ color: 'rgba(0,0,0,.25)' }}/>}
                                    placeholder="Username"
                                />
                            </FormItem>
                            <FormItem
                                label="Password"
                                name={'password'}
                                rules={[{ required: true, message: 'Please input your Password!' }]}
                            >
                                <Input
                                    prefix={<LockOutlined style={{ color: 'rgba(0,0,0,.25)' }}/>}
                                    type="password"
                                    placeholder="Password"
                                />
                            </FormItem>
                            <FormItem name={'remember'}>
                                <Checkbox>Remember me</Checkbox>
                            </FormItem>
                            <NavLink
                                className="login-form-forgot pull-right text-primary"
                                style={{ lineHeight: '36px' }}
                                to="/"
                            >
                                Forgot password?
                            </NavLink>
                            <div className="form-actions">
                                <Button type="primary" htmlType="submit" className="login-form-button"
                                        onClick={handleSubmit}>
                                    Sign in
                                </Button>
                            </div>
                        </Form>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default memo(LoginLayout)
