import { BackTop, Layout } from 'antd'
import React, { memo } from 'react'
import Header from '../header/Header'
import Sidebar from '../sidebar/Siderbar'
import { useSelector } from 'react-redux'
import { globalSel } from '../../../store/ducks/global'

function MainLayout({ children }) {
    const currentUser = useSelector(globalSel.currentUserSelector)

    return (
        <Layout className={'settings__borderLess settings__squaredBorders'}>
            <BackTop/>
            {currentUser?.role?.dashboardType === 1 ? <Sidebar/> : null}
            <Layout>
                <Layout.Header>
                    <Header/>
                </Layout.Header>
                <Layout.Content style={{ height: '100%', position: 'relative' }}>
                    <div className={`${currentUser?.role?.dashboardType === 1 ? 'utils__content' : ''}`}>
                        {children}
                    </div>
                </Layout.Content>
            </Layout>
        </Layout>
    )
}

export default memo(MainLayout)
