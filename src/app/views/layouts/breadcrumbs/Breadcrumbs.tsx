import { reduce } from 'lodash'
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { SIDEBAR_ITEMS } from '../../../constants'
import styles from './style.module.scss'

class Breadcrumbs extends React.Component {
    public state = {
        breadcrumb: [],
    }

    public componentDidMount() {
        this.setBreadcrumbs(this.props)
    }

    public componentWillReceiveProps(newProps) {
        this.setBreadcrumbs(newProps)
    }

    public setBreadcrumbs = props => {
        this.setState({
            breadcrumb: this.getBreadcrumb(props, SIDEBAR_ITEMS),
        })
    }

    public getPath(data, url, parents = []) {
        const items = reduce(
            data,
            (result, entry) => {
                if (result.length) {
                    return result
                }

                if (entry.url === url) {
                    return [entry].concat(parents)
                }
                if (entry.children) {
                    const nested = this.getPath(entry.children, url, [entry].concat(parents))
                    return (result || []).concat(nested.filter(e => !!e))
                }
                return result
            },
            [],
        )

        return items.length > 0 ? items : [false]
    }

    public getBreadcrumb = (props, items) => {
        const [activeMenuItem, ...path] = this.getPath(items, props.router.location.pathname)

        if (activeMenuItem && path.length) {
            return path.reverse().map((item, index) => {
                if (index === path.length - 1) {
                    return (
                        <span key={item.key}>
                            <span className={`${styles.arrow} text-muted`}/>
                            <span className="text-muted">
                                <Link to={item.url} className="text-muted">{item.title}</Link>
                            </span>
                            <span className={styles.arrow}/>
                            <strong>{activeMenuItem.title}</strong>
                        </span>
                    )
                }
                return (
                    <span key={item.key}>
                        <span className={`${styles.arrow} text-muted`}/>
                        <span className="text-muted">{item.title}</span>
                    </span>
                )
            })
        }
        return (
            <span>
                <span className={styles.arrow}/>
                <strong>{activeMenuItem.title}</strong>
            </span>
        )
    }

    public render() {
        const { breadcrumb } = this.state

        return (
            <div className={styles.breadcrumbs}>
                <div className={styles.path}>
                    <Link to="/" className="text-muted">
                        <i className="icmn-home topbar__dropdownIcon"/>
                    </Link>
                    {breadcrumb}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    router: state.router,
})

export default connect(mapStateToProps, null)(Breadcrumbs)
