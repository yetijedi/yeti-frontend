import { Layout, Menu } from 'antd'
import 'rc-drawer/assets/index.css'
import React, { memo } from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import { Link } from 'react-router-dom'
import { SIDEBAR_ITEMS, METHODS } from '../../../constants'
import styles from './style.module.scss'
import { useSelector } from 'react-redux'
import { globalSel } from '../../../store/ducks/global'
import { checkUserAccess } from '../../../services/checkUserAccess'

const { Sider } = Layout

function Sidebar() {
    const { get: GET } = METHODS
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)

    const onRenderThumbVertical = ({ style, ...props }) => (
        <div
            {...props}
            style={{
                ...style,
                width: '4px',
                borderRadius: 'inherit',
                backgroundColor: '#c5cdd2',
                left: '1px',
            }}
        />
    )


    const menuItems = SIDEBAR_ITEMS
        .map(({
                  key,
                  url,
                  icon,
                  title,
                  resourceCode,
              }) => {
            const hasAccessToShow = checkUserAccess(currentUser, commonSettings, resourceCode, GET)
            if (resourceCode && !hasAccessToShow) return null
            return (
                <Menu.Item key={key}>
                    <Link to={url}>
                        <i className={`${styles.icon} ${icon} icon-collapsed-hidden`}/>
                        <span className={styles.title}>{title}</span>
                    </Link>
                </Menu.Item>
            )
        })

    return (
        <Sider
            width={256}
            collapsible={true}
            breakpoint={'lg'}
            className={`${styles.menu} ${styles.light}`}>
            <div className={styles.logo}>
                <div className={styles.logoContainer}>
                    <img src={'resources/images/logo-inverse.png'} alt=""/>
                </div>
            </div>
            <Scrollbars
                className={styles.scrollbarDesktop}
                renderThumbVertical={onRenderThumbVertical}>
                <Menu
                    theme={'light'}
                    mode="inline"
                    className={styles.navigation}>
                    {menuItems}
                </Menu>
            </Scrollbars>
        </Sider>
    )
}

export default memo(Sidebar)
