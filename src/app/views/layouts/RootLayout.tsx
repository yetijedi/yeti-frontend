import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect } from 'react-router'
import { globalSel } from '../../store/ducks/global'
import Workspace from '../pages/workspace'

function RootLayout() {
    const currentUser = useSelector(globalSel.currentUserSelector)

    if (currentUser && currentUser.role) {
        switch (currentUser.role.dashboardType) {
            case 1:
                return <Redirect to="/users"/>
            case 2:
                return <Workspace/>
            default:
                return null
        }
    }

    return null
}

export default RootLayout
