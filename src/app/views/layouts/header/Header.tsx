import React from 'react'
import HomeMenu from './HomeMenu/HomeMenu'
import LanguageSelector from './LanguageSelector/LanguageSelector'
import ProfileMenu from './ProfileMenu/ProfileMenu'
import styles from './style.module.scss'
import { Link, useLocation } from 'react-router-dom'
import { Button } from 'antd'
import { useSelector } from 'react-redux'
import { globalSel } from '../../../store/ducks/global'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { METHODS } from '../../../constants'

function buttonCreator(name, path, user, settings) {
    const { post: POST } = METHODS
    const hasAccess = checkUserAccess(user, settings, name.toUpperCase(), POST)

    return (
        <>
            {
                hasAccess
                    ? (
                        <Button type="primary">
                            <Link to={`${path}/create`}>
                                {`Create ${name}`}
                            </Link>
                        </Button>
                    )
                    : null
            }

        </>
    )
}

const Header = () => {

    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { pathname } = useLocation()

    const renderCreateButtons: any = (path) => {
        switch (pathname) {
            case '/users':
                return buttonCreator('User', pathname, currentUser, commonSettings)
            case '/products':
                return buttonCreator('Product', pathname, currentUser, commonSettings)
            case '/shops':
                return buttonCreator('Shop', pathname, currentUser, commonSettings)
            case '/categories':
                return buttonCreator('Category', pathname, currentUser, commonSettings)
            default:
                return null
        }
    }

    return (
        <div className={styles.header}>
            <div className="mr-auto">
                {currentUser?.role?.dashboardType === 1 ? renderCreateButtons() : null}
            </div>
            <div className="mr-4">
                <LanguageSelector/>
            </div>
            <div className="mr-4">
                <HomeMenu/>
            </div>
            {currentUser && <ProfileMenu currentUser={currentUser}/>}
        </div>
    )
}

export default Header
