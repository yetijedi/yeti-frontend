import { Dropdown, Menu } from 'antd/lib/index'
import React from 'react'
// import { connect } from 'react-redux'
import styles from './style.module.scss'

class LanguageSelector extends React.Component {
    public changeLang = ({ key }) => {
        // console.log(key);
    };

    public render() {
        const langMenu = (
            <Menu className={styles.menu} onClick={this.changeLang}>
                <Menu.Item key="en-US">
                    <span role="img" aria-label="English" className="mr-2">
                        🇬🇧
                    </span>
                    English
                </Menu.Item>
                <Menu.Item key="fr-FR">
                    <span role="img" aria-label="French" className="mr-2">
                        🇫🇷
                    </span>
                    Armenia
                </Menu.Item>
                <Menu.Item key="ru-RU">
                    <span role="img" aria-label="Русский" className="mr-2">
                         🇷🇺
                    </span>
                    Русский
                </Menu.Item>
            </Menu>
        );
        return (
            <Dropdown overlay={langMenu} trigger={['click']}>
                <div className={styles.dropdown}>
                    <strong className="text-uppercase">EN</strong>
                </div>
            </Dropdown>
        )
    }
}

export default LanguageSelector;
