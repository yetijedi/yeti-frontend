import { Avatar, Dropdown, Menu } from 'antd'
import React, { memo, useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { globalOp } from '../../../../store/ducks/global'
import styles from './style.module.scss'
import { UserOutlined } from '@ant-design/icons'

function ProfileMenu({ currentUser }) {
    const dispatch = useDispatch()
    const { name, email, phone, role: { name: roleName } } = currentUser

    const handleLogout = useCallback(() => {
        dispatch(globalOp.logoutAsync())
    }, [])

    const menu = (
        <Menu selectable={false}>
            <Menu.Item>
                <strong>
                    {`Hello ${name || 'Anonymous'}`}
                </strong>
                {/* In future if there will be billing plans */}
                {/*<div>*/}
                {/*    <strong className="mr-1">*/}
                {/*        Billing Plan:{ ' ' }*/}
                {/*    </strong>*/}
                {/*    Professional*/}
                {/*</div>*/}
                <div>
                    <strong>
                        Role:{' '}
                    </strong>
                    {roleName}
                </div>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item>
                <div>
                    <strong>
                        Email:{' '}
                    </strong>
                    {email || '-'}
                    <br/>
                    <strong>
                        Phone:{' '}
                    </strong>
                    {phone}
                </div>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item>
                <Link to="/">
                    <i className={`${styles.menuIcon} icmn-cog`}/>
                    Settings
                </Link>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item>
                <div onClick={handleLogout}>
                    <i className={`${styles.menuIcon} icmn-exit`}/>
                    Logout
                </div>
            </Menu.Item>
        </Menu>
    )
    return (
        <Dropdown overlay={menu} trigger={['click']}>
            <div className={styles.dropdown}>
                <Avatar className={styles.avatar} shape="square" size="large" icon={<UserOutlined/>}/>
            </div>
        </Dropdown>
    )
}

export default memo(ProfileMenu)
