import { Table, Tag, Button } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import React, { memo, useEffect, useMemo } from 'react'
import { productsOp, productsSel } from '../../../store/ducks/products'
import { useDispatch, useSelector } from 'react-redux'
import { DEFAULT_CURRENCY, RESOURCE_CODES, METHODS } from '../../../constants'
import { thousandsFormatter } from '../../../services/formatters'
import { Link } from 'react-router-dom'
import { notify } from '../../../services/notifier'
import { globalSel } from '../../../store/ducks/global'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { confirmationDialog } from '../../components/common/ConfirmationDialog'

function ProductsList() {
    const dispatch = useDispatch()
    const { products } = useSelector(productsSel.productSelector)
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { put: PUT, delete: DELETE } = METHODS
    const { products: PRODUCT } = RESOURCE_CODES

    const disabledActions = method => checkUserAccess(currentUser, commonSettings, PRODUCT, method)

    const columns = useMemo(() => ([
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Available',
            dataIndex: 'available',
            key: 'available',
        },
        {
            title: 'Sell Price',
            dataIndex: 'sell_price',
            key: 'sell_price',
            // Experimental
            render: tag => (
                <Tag color="geekblue">
                    {thousandsFormatter(tag)} {DEFAULT_CURRENCY}
                </Tag>
            ),
        },
        {
            title: 'Action',
            key: 'action',
            render: product => {
                return (
                    <>
                        <Button
                            disabled={!disabledActions(PUT)}
                            type="link">
                            <Link to={`/products/update/${product.id}`}>
                                <EditOutlined/>
                            </Link>
                        </Button>
                        <Button
                            disabled={!disabledActions(DELETE)}
                            onClick={() => confirmationDialog(() => fetchDeleteProduct(product.id), product.name)}
                            type="link" danger={true}>
                            <DeleteOutlined/>
                        </Button>
                    </>
                )
            },
        },
    ]), [])

    const fetchDeleteProduct = async productId => {
        try {
            await productsOp.deleteProductAsync(productId)
            await dispatch(productsOp.getProductsAsync())
            notify('Product deleted successfully!')
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        dispatch(productsOp.getProductsAsync())
    }, [])

    return (
        <Table
            dataSource={products}
            columns={columns}
            pagination={false}
        />
    )
}

export default memo(ProductsList)
