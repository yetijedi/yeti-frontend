import React, { memo, useCallback, useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router'
import { Input, InputNumber, Button, Modal, Upload, Form } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import styles from './style.module.scss'
import { Link } from 'react-router-dom'
import { productsOp } from '../../../../store/ducks/products'
import { notify } from '../../../../services/notifier'
import UnitTypeSelect from '../../../components/common/UnitTypeSelect'
import CategorySelect from '../../../components/common/CategorySelect'
import ShopSelect from '../../../components/common/ShopSelect'

const { TextArea } = Input
const FormItem = Form.Item

const validateMessages = {
    // tslint:disable-next-line:no-invalid-template-strings
    required: '${label} is required!',
    types: {
        // tslint:disable-next-line:no-invalid-template-strings
        number: '${label} is not a validate number!',
    },
}

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result)
        reader.onerror = error => reject(error)
    })
}

function CreateUpdateProduct() {
    const [form] = Form.useForm()
    const { id: productId } = useParams()
    const history = useHistory()

    const [previewImage, setPreviewImage] = useState('')
    const [imagePreviewVisible, setImagePreviewVisible] = useState(false)
    const [productImage, setProductImage] = useState([])

    const fetchSingleProduct = async id => {
        try {
            const {
                data: {
                    category_id,
                    shop_id,
                    name,
                    sell_price,
                    native_price,
                    description,
                    available,
                    unit_type_id,
                },
            } = await productsOp.getSingleProductAsync(id)

            form.setFieldsValue({
                category_id,
                shop_id,
                name,
                sell_price,
                native_price,
                description,
                available,
                unit_type_id,
            })
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        if (productId) {
            fetchSingleProduct(productId)
        }
    }, [])

    const handleImagePreviewVisible = useCallback(() => setImagePreviewVisible(false), [])

    const handleImagePreview = useCallback(async file => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj)
        }

        setPreviewImage(file.url || file.preview)
        setImagePreviewVisible(true)
    }, [])

    const handleProductImageUpload = useCallback(({ fileList }) => setProductImage(fileList), [])

    const handleCreateUpdateProduct = useCallback(async e => {
        e.preventDefault()
        try {
            const validFields = await form.validateFields()
            if (productId) {
                await productsOp.updateProductAsync({
                    ...validFields,
                    // tslint:disable-next-line:radix
                    id: parseInt(productId),
                })
                notify('Product updated successfully')
            } else {
                await productsOp.createProductAsync({ ...validFields })
                notify('Product created successfully')
            }

            history.push('/products')

        } catch (err) {
            // console.log(err)
            notify('Something went wrong!', 'error')
        }
    }, [])

    return (
        <div>
            <div className="card">
                <div className="card-header">
                    <div className="utils__title">
                        <strong>{productId ? 'Product Update' : 'Product Create'}</strong>
                    </div>
                </div>
                <div className="card-body">
                    <h4 className="text-black mb-3">
                        <strong>Main Parameters</strong>
                    </h4>
                    <Form validateMessages={validateMessages} form={form} layout="vertical">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true }]}
                                                name="name"
                                                label="Name">
                                                <Input placeholder="Product name"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ type: 'number', required: true }]}
                                                name="available"
                                                className="w-100"
                                                label="Available">
                                                <InputNumber
                                                    className="w-100"
                                                    placeholder="Product availability"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <CategorySelect/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <ShopSelect/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <UnitTypeSelect/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <FormItem
                                                name="description"
                                                label="Description">
                                                <TextArea rows={3}/>
                                            </FormItem>
                                        </div>
                                        <h4 className="text-black mt-2 mb-3">
                                            <strong>Pricing</strong>
                                        </h4>
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <FormItem
                                                        className="w-100"
                                                        rules={[{ type: 'number' }]}
                                                        name="sell_price"
                                                        label="Sell Price">
                                                        <InputNumber
                                                            className="w-100"
                                                            placeholder="Sell Price"/>
                                                    </FormItem>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <FormItem
                                                        className="w-100"
                                                        rules={[{ type: 'number', required: true }]}
                                                        name="native_price"
                                                        label="Native Price">
                                                        <InputNumber
                                                            className="w-100"
                                                            placeholder="Native Price"/>
                                                    </FormItem>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-actions">
                                                    <Button
                                                        onClick={handleCreateUpdateProduct}
                                                        type="primary"
                                                        className="mr-2">
                                                        {productId ? 'Update Product' : 'Save Product'}
                                                    </Button>
                                                    <Button type="default">
                                                        <Link to="/products">
                                                            Cancel
                                                        </Link>
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div
                                    className="d-flex border align-items-center justify-content-center"
                                    style={{
                                        height: 'calc(100%)',
                                    }}
                                >
                                    {/*After API implementation UNCOMMENT*/}
                                    {/*<FormItem name="productImage">*/}
                                    <Upload
                                        className={`${styles.uploadArea} d-flex align-items-center justify-content-center w-100`}
                                        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                        listType="picture-card"
                                        fileList={productImage}
                                        onPreview={handleImagePreview}
                                        onChange={handleProductImageUpload}
                                    >
                                        {
                                            productImage.length
                                                ? null
                                                : (
                                                    <div>
                                                        <p className="ant-upload-drag-icon text-center mb-3">
                                                            <PlusOutlined className={styles.uploadImageIcon}/>
                                                        </p>
                                                        <p className="ant-upload-text text-center">Upload Product
                                                            Image</p>
                                                    </div>
                                                )
                                        }
                                    </Upload>
                                    {/*</FormItem>*/}
                                    <Modal
                                        visible={imagePreviewVisible}
                                        footer={null}
                                        onCancel={handleImagePreviewVisible}>
                                        <img alt={previewImage} style={{ width: '100%' }} src={previewImage}/>
                                    </Modal>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default memo(CreateUpdateProduct)
