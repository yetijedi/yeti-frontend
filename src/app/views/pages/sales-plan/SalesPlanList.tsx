import React  from "react";
import { connect } from "react-redux";
import { salesPlanOperations, salesPlanSelectors } from "../../../store/ducks/sales-plan";

class SalesPlanList extends React.PureComponent<IProps, IState> {
    public componentDidMount(): void {
        this.props.getSalesPlan();
    }

    public render() {
        return null;
    }
}

interface IProps {
    salesPlan: any,
    getSalesPlan: () => {}
}

interface IState {

}

const mapStateToProps = state => ({
    salesPlan: salesPlanSelectors.salesPlanSelector(state)
});

const dispatchToProps = dispatch => ({
    getSalesPlan: () => dispatch(salesPlanOperations.getSalesPlanAsync())
});

export default connect(mapStateToProps, dispatchToProps)(SalesPlanList);
