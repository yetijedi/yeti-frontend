import React, { memo, useCallback, useEffect } from 'react'
import { useParams, useHistory } from 'react-router'
import { Input, Button, Form } from 'antd'
import UnitTypeSelect from '../../../components/common/UnitTypeSelect'
import { Link } from 'react-router-dom'
import { categoriesOp } from '../../../../store/ducks/categories'
import { notify } from '../../../../services/notifier'

const { TextArea } = Input
const FormItem = Form.Item

function CreateUpdateCategory() {
    const [form] = Form.useForm()
    const { id: categoryId } = useParams()
    const history = useHistory()

    const fetchSingleCategory = async id => {
        try {
            const {
                data: {
                    name,
                    description,
                    unit_type_id,
                },
            } = await categoriesOp.getSingleCategoryAsync(id)

            form.setFieldsValue({
                name,
                description,
                unit_type_id
            })
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        if (categoryId) {
            fetchSingleCategory(categoryId)
        }
    }, [])

    const handleCreateUpdateCategory = useCallback(async e => {
        e.preventDefault()

        try {
            const validFields = await form.validateFields()
            if (categoryId) {
                await categoriesOp.updateCategoryAsync({
                    ...validFields,
                    // tslint:disable-next-line:radix
                    id: parseInt(categoryId),
                })
                notify('Category updated successfully')
            } else {
                await categoriesOp.createCategoryAsync({ ...validFields })
                notify('Category created successfully')
            }

            history.push('/categories')
        } catch (err) {
            // console.log(err)
            notify('Something went wrong!', 'error')
        }
    }, [])

    return (
        <div>
            <div className="card">
                <div className="card-header">
                    <div className="utils__title">
                        <strong>{categoryId ? 'Category Update' : 'Category Create'}</strong>
                    </div>
                </div>
                <div className="card-body">
                    <Form form={form} layout="vertical">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true, message: 'Category name is required' }]}
                                                name="name"
                                                label="Name">
                                                <Input placeholder="Category name"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <UnitTypeSelect/>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <FormItem
                                                name="description"
                                                label="Description">
                                                <TextArea rows={3}/>
                                            </FormItem>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-actions">
                                                    <Button
                                                        onClick={handleCreateUpdateCategory}
                                                        type="primary"
                                                        className="mr-2">
                                                        {categoryId ? 'Update Category' : 'Save Category'}
                                                    </Button>
                                                    <Button type="default">
                                                        <Link to="/categories">
                                                            Cancel
                                                        </Link>
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default memo(CreateUpdateCategory)
