import { Button, Table } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import React, { useEffect, memo, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { categoriesSel, categoriesOp } from '../../../store/ducks/categories'
import { Link } from 'react-router-dom'
import { notify } from '../../../services/notifier'
import { dateFormatter } from '../../../services/formatters'
import { globalSel } from '../../../store/ducks/global'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { METHODS, RESOURCE_CODES } from '../../../constants'
import { confirmationDialog } from '../../components/common/ConfirmationDialog'

function CategoriesList() {
    const [categoriesRows, setCategoriesRows] = useState([])

    const dispatch = useDispatch()
    const { categories } = useSelector(categoriesSel.categorySelector)
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { put: PUT, delete: DELETE } = METHODS
    const { categories: CATEGORY } = RESOURCE_CODES

    const disabledActions = method => checkUserAccess(currentUser, commonSettings, CATEGORY, method)


    const columns = useMemo(() => ([
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Created At',
            dataIndex: 'created_at',
            key: 'created_at',
            render: date => dateFormatter(date),
        },
        {
            title: 'Action',
            key: 'action',
            render: category => {
                return (
                    <>
                        <Button
                            disabled={!disabledActions(PUT)}
                            type="link">
                            <Link to={`/categories/update/${category.id}`}>
                                <EditOutlined/>
                            </Link>
                        </Button>
                        <Button
                            disabled={!disabledActions(DELETE)}
                            onClick={() => confirmationDialog(() => fetchDeleteCategory(category.id), category.name)}
                            type="link"
                            danger={true}>
                            <DeleteOutlined/>
                        </Button>
                    </>
                )
            },
        },
    ]), [])

    const fetchDeleteCategory = async categoryId => {
        try {
            await categoriesOp.deleteCategoryAsync(categoryId)
            await dispatch(categoriesOp.getCategoriesAsync())
            notify('Category deleted successfully!')
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        dispatch(categoriesOp.getCategoriesAsync())
    }, [])

    // With this method Component rendering 4 times
    // It's correct one
    useEffect(() => {
        if (commonSettings) {
            const rows = categories.filter(s => s.id !== commonSettings.defaultCategoryId)
            setCategoriesRows(rows)
        }
    }, [categories, commonSettings])

    return (
        <Table
            // WITH this method component rendering 3 times
            // dataSource={[...categories.filter(s => s.id !== commonSettings.defaultCategoryId)]}

            dataSource={categoriesRows}
            columns={columns}
            pagination={false}
        />
    )
}

export default memo(CategoriesList)
