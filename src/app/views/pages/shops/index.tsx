import { Button, Table } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import React, { memo, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { shopsOp, shopsSel } from '../../../store/ducks/shops'
import { thousandsFormatter } from '../../../services/formatters'
import { Link } from 'react-router-dom'
import { notify } from '../../../services/notifier'
import { DEFAULT_CURRENCY, METHODS, RESOURCE_CODES } from '../../../constants'
import { globalSel } from '../../../store/ducks/global'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { confirmationDialog } from '../../components/common/ConfirmationDialog'

function ShopsList() {
    const [shopRows, setShopRows] = useState([])

    const dispatch = useDispatch()
    const { shops } = useSelector(shopsSel.shopSelector)
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { put: PUT, delete: DELETE } = METHODS
    const { shops: SHOP } = RESOURCE_CODES

    const disabledActions = method => checkUserAccess(currentUser, commonSettings, SHOP, method)

    const columns = useMemo(() => ([
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Current Balance',
            dataIndex: 'current_balance',
            key: 'current_balance',
            render: balance => `${thousandsFormatter(balance)} ${DEFAULT_CURRENCY}`,
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Working Days',
            dataIndex: 'working_days',
            key: 'working_days',
        },
        {
            title: 'Open At',
            dataIndex: 'open_at',
            key: 'open_at',
        },
        {
            title: 'Close At',
            dataIndex: 'close_at',
            key: 'close_at',
        },
        {
            title: 'Action',
            key: 'action',
            render: shop => {
                return (
                    <>
                        <Button
                            disabled={!disabledActions(PUT)}
                            type="link">
                            <Link to={`/shops/update/${shop.id}`}>
                                <EditOutlined/>
                            </Link>
                        </Button>
                        <Button
                            disabled={!disabledActions(DELETE)}
                            onClick={() => confirmationDialog(() => fetchDeleteShop(shop.id), shop.name)}
                            type="link" danger={true}>
                            <DeleteOutlined/>
                        </Button>
                    </>
                )
            },
        },
    ]), [])

    const fetchDeleteShop = async shopId => {
        try {
            await shopsOp.deleteShopAsync(shopId)
            await dispatch(shopsOp.getShopsAsync())
            notify('Shop deleted successfully!')
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        dispatch(shopsOp.getShopsAsync())
    }, [])

    // With this method Component rendering 4 times
    // It's correct one
    useEffect(() => {
        if (commonSettings) {
            const rows = shops.filter(s => s.id !== commonSettings.defaultShopId)
            setShopRows(rows)
        }
    }, [shops, commonSettings])

    return (
        <Table
            // WITH this method component rendering 3 times
            // dataSource={[...shops.filter(s => s.id !== commonSettings.defaultShopId)]}

            dataSource={shopRows}
            columns={columns}
            pagination={false}
        />
    )
}

export default memo(ShopsList)
