import React, { memo, useCallback, useEffect } from 'react'
import { useParams, useHistory } from 'react-router'
import { Input, Button, Form, InputNumber, TimePicker } from 'antd'
import { Link } from 'react-router-dom'
import { shopsOp } from '../../../../store/ducks/shops'
import { notify } from '../../../../services/notifier'
import moment from 'moment'

const { TextArea } = Input
const FormItem = Form.Item

function CreateUpdateShop() {
    const [form] = Form.useForm()
    const { id: shopId } = useParams()
    const history = useHistory()

    const fetchSingleShop = async id => {
        try {
            const {
                data: {
                    name,
                    description,
                    working_days,
                    current_balance,
                    open_at,
                    close_at,

                },
            } = await shopsOp.getSingleShopAsync(id)

            form.setFieldsValue({
                name,
                description,
                working_days,
                current_balance,
                open_at: moment(open_at),
                close_at: moment(close_at),
            })
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        if (shopId) {
            fetchSingleShop(shopId)
        }
    }, [])

    const handleCreateUpdateShop = useCallback(async e => {
        e.preventDefault()

        try {
            const validFields = await form.validateFields()
            if (validFields.open_at) {
                validFields.open_at = validFields.open_at.format('HH:mm:ss')
            }

            if (validFields.close_at) {
                validFields.close_at = validFields.close_at.format('HH:mm:ss')
            }

            if (shopId) {
                await shopsOp.updateShopAsync({
                    ...validFields,
                    // tslint:disable-next-line:radix
                    id: parseInt(shopId),
                })
                notify('Shop updated successfully')
            } else {
                await shopsOp.createShopAsync({ ...validFields })
                notify('Shop created successfully')
            }

            history.push('/shops')
        } catch (err) {
            // console.log(err)
            notify('Something went wrong!', 'error')
        }
    }, [])

    return (
        <div>
            <div className="card">
                <div className="card-header">
                    <div className="utils__title">
                        <strong>{shopId ? 'Shop Update' : 'Shop Create'}</strong>
                    </div>
                </div>
                <div className="card-body">
                    <h4 className="text-black mb-3">
                        <strong>Main Parameters</strong>
                    </h4>
                    <Form form={form} layout="vertical">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true }]}
                                                name="name"
                                                label="Name">
                                                <Input placeholder="Shop name"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ type: 'number', required: true }]}
                                                name="working_days"
                                                className="w-100"
                                                label="Working Days">
                                                <InputNumber
                                                    className="w-100"
                                                    placeholder="Shop working days"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ type: 'number', required: true }]}
                                                name="current_balance"
                                                className="w-100"
                                                label="Current Balance">
                                                <InputNumber
                                                    className="w-100"
                                                    placeholder="Shop current balance"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="form-group">
                                            <FormItem
                                                name="description"
                                                label="Description">
                                                <TextArea rows={3}/>
                                            </FormItem>
                                        </div>
                                        <h4 className="text-black mt-2 mb-3">
                                            <strong>Working Hours</strong>
                                        </h4>
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <FormItem
                                                        name="open_at"
                                                        label="Open At">
                                                        <TimePicker className="w-100"
                                                                    minuteStep={15}
                                                                    secondStep={10}/>
                                                    </FormItem>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <FormItem
                                                        name="close_at"
                                                        label="Close At">
                                                        <TimePicker className="w-100"
                                                                    minuteStep={15}
                                                                    secondStep={10}/>
                                                    </FormItem>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-actions">
                                                    <Button
                                                        onClick={handleCreateUpdateShop}
                                                        type="primary"
                                                        className="mr-2">
                                                        {shopId ? 'Update Shop' : 'Save Shop'}
                                                    </Button>
                                                    <Button type="default">
                                                        <Link to="/shops">
                                                            Cancel
                                                        </Link>
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default memo(CreateUpdateShop)
