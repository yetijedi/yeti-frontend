import { Button, Table, Tag } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import React, { memo, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { globalSel } from '../../../store/ducks/global'
import { DEFAULT_CURRENCY, METHODS, RESOURCE_CODES } from '../../../constants'
import { ordersOp, ordersSel } from '../../../store/ducks/orders'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { dateFormatter, thousandsFormatter } from '../../../services/formatters'
import { notify } from '../../../services/notifier'
import { Link } from 'react-router-dom'
import OrderItem from './OrderItem'
import { confirmationDialog } from '../../components/common/ConfirmationDialog'

function OrdersList() {
    const dispatch = useDispatch()
    const { orders } = useSelector(ordersSel.orderSelector)
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { put: PUT, delete: DELETE } = METHODS
    const { orders: ORDERS } = RESOURCE_CODES

    const disabledActions = method => checkUserAccess(currentUser, commonSettings, ORDERS, method)

    const columns = useMemo(() => ([
        {
            title: 'Order Id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Total Amount',
            dataIndex: 'total_amount',
            key: 'total_amount',
            render: amount => (
                <Tag color="green">
                    {thousandsFormatter(amount)} {DEFAULT_CURRENCY}
                </Tag>
            ),
        },
        {
            title: 'Updated At',
            dataIndex: 'updated_at',
            key: 'updated_at',
            render: date => dateFormatter(date),
        },
        {
            title: 'Created At',
            dataIndex: 'created_at',
            key: 'created_at',
            render: date => dateFormatter(date),
        },
        {
            title: 'Action',
            key: 'action',
            render: order => {
                return (
                    <>
                        <Button
                            disabled={!disabledActions(PUT)}
                            type="link">
                            <Link to={`/orders/update/${order.id}`}>
                                <EditOutlined/>
                            </Link>
                        </Button>
                        <Button
                            disabled={!disabledActions(DELETE)}
                            onClick={() => confirmationDialog(() => fetchDeleteOrders(order.id), `Order ${order.id}`)}
                            type="link"
                            danger={true}>
                            <DeleteOutlined/>
                        </Button>
                    </>
                )
            },
        },
    ]), [])

    const fetchDeleteOrders = async orderId => {
        try {
            await ordersOp.deleteOrderAsync(orderId)
            await dispatch(ordersOp.getOrdersAsync())
            notify('Order deleted successfully!')
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        dispatch(ordersOp.getOrdersAsync())
    }, [])

    return (
        <>
            <Table
                expandable={{
                    expandedRowRender: record => <OrderItem item={record}/>,
                }}
                columns={columns}
                pagination={false}
                dataSource={orders}/>
        </>
    )
}

export default memo(OrdersList)
