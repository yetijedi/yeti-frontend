import { Table, Tag } from 'antd'
import React, { FC, memo, useMemo } from 'react'
import { DEFAULT_CURRENCY } from '../../../../constants'
import { thousandsFormatter } from '../../../../services/formatters'

const columns = [
    {
        title: 'Product Name',
        dataIndex: 'name',
        key: 'name',
    },
    {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity',
    },
    {
        title: 'Price * Quantity',
        key: 'total_price',
        render: record => (
            <Tag color="geekblue">
                {thousandsFormatter(record.sell_price * record.quantity)} {DEFAULT_CURRENCY}
            </Tag>
        ),
    },
]

const OrderItem: FC<IProps> = ({ item }) => {

    const orderInfo = useMemo(() => item.order_info.map(o => ({ ...o, key: o.id })), [item])

    return (
        <>
            <Table
                columns={columns}
                pagination={false}
                dataSource={orderInfo}/>
        </>
    )
}

interface IProps {
    item: any
}

export default memo(OrderItem)
