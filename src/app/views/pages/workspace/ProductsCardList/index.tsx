import React, { FC, memo, useCallback, useMemo } from 'react'
import ProductCardItem from './ProductsCardItem'
import { useDispatch } from 'react-redux'
import { workspaceOp } from '../../../../store/ducks/workspace'

const ProductsCardList: FC<Props> = ({ products, categoryId, orderId }) => {
    const dispatch = useDispatch()

    const handleSelectedProduct = useCallback(product => {
        dispatch(workspaceOp.setBasket({ ...product }))
    }, [])

    const renderProducts = useMemo(() => {
        if (products) {
            const filteredProducts = categoryId !== -1
                ? products.filter(p => p.category_id === categoryId)
                : products

            return (
                filteredProducts.length
                    ? (
                        filteredProducts.map(p => (
                                <div key={p.id}
                                     className={`col-6 col-sm-6 col-md-${orderId ? '6' : '4'} pl-1 pr-1`}>
                                    <ProductCardItem
                                        productOnClick={() => handleSelectedProduct(p)}
                                        productSellPrice={p.sell_price}
                                        productName={p.name}
                                        productImg={''}
                                        productDescription={p.description}
                                    />
                                </div>
                            ),
                        ))
                    : <div className="col-12 p-5">No Products</div>
            )
        }

        return null

    }, [products, categoryId, handleSelectedProduct])

    return (
        <div className="col-12">
            <div className="row no-gutters">
                {renderProducts}
            </div>
        </div>
    )
}

interface Props {
    products: any[],
    categoryId: number,
    orderId?: string,
}

export default memo(ProductsCardList)
