import React, { FC, memo } from 'react'
import { connect } from 'react-redux'
import { DEFAULT_CURRENCY } from '../../../../../constants'
import styles from './style.module.scss'

const ProductsCardItem: FC<IProps> = (
    {
        // productStatus,
        productImg,
        productSellPrice,
        productName,
        productDescription,
        productOnClick
    },
) => {
    return (
        <div onClick={productOnClick} className={styles.productCard}>
            <div className={styles.img}>

                {/* PRODUCT STATUS NEW OR SALE */}

                {/*{productStatus === 'new' && (*/}
                {/*    <div className={styles.status}>*/}
                {/*        <span className={styles.statusTitle}>New</span>*/}
                {/*    </div>*/}
                {/*)}*/}

                {/*FAVORITE PRODUCT OR MOST SOLD PRODUCT*/}

                {/*<div className={styles.like}>*/}
                {/*    <i className="icmn-heart"/>*/}
                {/*</div>*/}
                <div>
                    <img src={productImg ? productImg : '../../../../../../../resources/images/ecommerce/unnamed.png'} alt=""/>
                </div>
            </div>
            <div className={styles.title}>
                <div className={styles.productName}>{productName}</div>
                <div className={styles.price}>
                    {`${productSellPrice || ''} ${productSellPrice ? DEFAULT_CURRENCY : ''}`}
                    {/* WHEN SALE SHOW THIS ROW */}
                    {/*<div className={styles.oldPrice}>{productOldPrice}</div>*/}
                </div>
            </div>
            <div className={styles.descr}>
                {`${productDescription}`}
            </div>
        </div>
    )
}

interface IProps {
    productImg: string,
    productName: string,
    productSellPrice: number,
    productDescription: string,
    productOnClick()
    // productStatus,
}

const mapStateToProps = state => ({})

const dispatchToProps = dispatch => ({})

export default connect(mapStateToProps, dispatchToProps)(memo(ProductsCardItem))
