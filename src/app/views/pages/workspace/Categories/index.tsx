import { Menu } from 'antd'
import React, { FC, memo } from 'react'
import styles from './style.module.scss'

const Categories: FC<ICategoriesProps> = ({ categories, handleCategoriesId }) => {

    const handleCategoryId = id => handleCategoriesId(id)

    return (
        <Menu
            mode="inline"
            defaultSelectedKeys={['-1']}
            className={styles.categoriesMenu}
        >
            <Menu.Item
                className={styles.categoriesMenuItem}
                onClick={() => handleCategoryId(-1)} key="-1">
                All
            </Menu.Item>
            {
                categories.map(c => (
                    <Menu.Item
                        className={styles.categoriesMenuItem}
                        onClick={() => handleCategoryId(c.id)} key={c.id}>{c.name}</Menu.Item>
                ))
            }
        </Menu>
    )
}

interface ICategoriesProps {
    categories: any[],

    handleCategoriesId(id)
}

export default memo(Categories)
