import { Avatar } from 'antd'
import React, { FC, memo } from 'react'
import styles from './style.module.scss'
import { UserOutlined } from '@ant-design/icons'

const ShopInfo: FC<any> = ({ shop }) => {
    return (
        <div className={`pt-2 pb-2 pl-3 pr-3 d-flex w-100 align-items-center ${styles['workspace-shop']}`}>
            <div className="mr-3">
                <Avatar size={44} icon={<UserOutlined/>}/>
            </div>
            <div>
                <div className={`${styles['workspace-shop_name']} mb-2`}>{shop.name}</div>
                <div className={styles['workspace-shop_address']}>{shop.address || 'Temporary address'}</div>
            </div>
        </div>
    )
}

export default memo(ShopInfo)
