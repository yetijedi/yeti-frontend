import { Progress } from 'antd'
import React, { FC, memo } from 'react'
import styles from './style.module.scss'

const DailyPlanning: FC<any> = () => {
    return (
        <div className={`${styles['workspace-daily-planning']} p-3 w-100`}>
            <div className={`${styles['workspace-daily-planning_title']}`}>Daily Progress</div>
            <Progress size="small" percent={30}/>
        </div>
    )
}

export default memo(DailyPlanning)
