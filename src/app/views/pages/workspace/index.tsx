import { Layout } from 'antd'
import React, { memo, useCallback, useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { useDispatch, useSelector } from 'react-redux'
import Categories from './Categories'
import DailyPlanning from './DailyPlanning'
import OrderPreview from './OrderPreview'
import ProductsCardList from './ProductsCardList'
import ShopInfo from './ShopInfo'
import styles from './style.module.scss'
import { workspaceOp, workspaceSel } from '../../../store/ducks/workspace'
import { ordersOp } from '../../../store/ducks/orders'
import { notify } from '../../../services/notifier'

// const { Search } = Input;

function Workspace() {
    const dispatch = useDispatch()
    const { id: orderId } = useParams()
    const [shopId, setShopId] = useState(undefined)
    const { shop, categories, products } = useSelector(workspaceSel.dataSelector)

    const fetchSingleOrder = async orderId => {
        try {
            const { data } = await ordersOp.getSingleOrderAsync(orderId)
            const orderInfo = data.order_info.map(o => ({ ...o, itemTotalPrice: o.quantity * o.sell_price }))
            setShopId(data.shop_id)
            dispatch(workspaceOp.setBasketForUpdate(orderInfo))
        } catch (err) {
            notify(err.message, 'error')
        }
    }

    useEffect(() => {
        if(orderId) {
            fetchSingleOrder(orderId)
        }

        dispatch(workspaceOp.fetchWorkspaceAsync())
    }, [])

    const [categoryId, setCategoryId] = useState(-1)
    const handleCategoriesId = useCallback(id => setCategoryId(id), [])

    return (
        <div>
            <Layout className={`${styles['workspace-layout']} settings__borderLess settings__squaredBorders`}>
                <Layout>
                    <Layout.Header className={`${styles['workspace-layout__header']}`}>
                        <div className="col-12">
                            <div className="row no-gutters">
                                <div className="col-5 d-flex align-items-center">
                                    <ShopInfo shop={shop}/>
                                </div>
                                <div className="col-7 d-flex align-items-center">
                                    <DailyPlanning/>
                                </div>
                            </div>
                        </div>
                    </Layout.Header>
                    <Layout.Content>
                        {/*<Search*/}
                        {/*    placeholder="Search Products"*/}
                        {/*    onSearch={value => console.log(value)}*/}
                        {/*/>*/}
                        <Layout>
                            <Layout.Sider width={140} className={styles['workspace-layout__aside']}>
                                <Categories
                                    categories={categories}
                                    handleCategoriesId={handleCategoriesId}/>
                            </Layout.Sider>
                            <Layout.Content className={styles['workspace-layout__content']}>
                                <ProductsCardList
                                    orderId={orderId}
                                    products={products}
                                    categoryId={categoryId}
                                />
                            </Layout.Content>
                        </Layout>
                    </Layout.Content>
                </Layout>
                <OrderPreview
                    shopId={shopId}
                    orderId={+orderId}/>
            </Layout>
        </div>
    )
}

export default memo(Workspace)
