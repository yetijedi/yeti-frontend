import { Button, Layout } from 'antd'
import React, { FC, memo, useCallback } from 'react'
import { Scrollbars } from 'react-custom-scrollbars'
import Basket from './Basket'
import styles from './style.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import { workspaceOp, workspaceSel } from '../../../../store/ducks/workspace'
import TotalAmount from '../../../components/common/TotalAmount'
import { notify } from '../../../../services/notifier'
import { ordersOp } from '../../../../store/ducks/orders'
import { useHistory } from 'react-router'
import { confirmationDialog } from '../../../components/common/ConfirmationDialog'

const { Sider } = Layout

interface IProps {
    orderId?: number,
    shopId?: number
}

const OrderPreview: FC<IProps> = ({ orderId, shopId }) => {
    const dispatch = useDispatch()
    const history = useHistory()
    const basket = useSelector(workspaceSel.basketSelector)
    const totalPrice = useSelector(workspaceSel.basketTotalPriceSelector)
    const { shop, lastOrderN } = useSelector(workspaceSel.dataSelector)

    const updateOrder = async () => {
        try {
            const orderInfo = basket.map(
                (
                    {
                        id,
                        quantity,
                        product_id,
                        name,
                        order_id,
                        sell_price,
                        native_price,
                    },
                ) => ({
                    id,
                    quantity,
                    product_id: product_id || id,
                    name,
                    order_id,
                    sell_price,
                    native_price,
                }))
            const order = {
                id: orderId,
                shop_id: shopId,
                total_amount: totalPrice,
                order_info: orderInfo,
            }
            await ordersOp.updateOrderAsync(order)
            notify('Order successfully updated')
            history.push('/orders')
        } catch (e) {
            notify(e.message, 'error')
        }
    }

    const createOrder = useCallback(() => {
        const orderInfo = basket.map(item => ({ product_id: item.id, quantity: item.quantity }))
        const order = {
            shop_id: shop.id,
            order_info: orderInfo,
        }
        dispatch(workspaceOp.fetchCreateOrderAsync(order))
    }, [basket])

    const attentionText = (
        <div>
            <ul>
                <li>Product availability</li>
                <li>Shop balance</li>
                <li>Daily plan</li>
                <li>Shop active plan</li>
            </ul>
            <p>will be recalculated!</p>
        </div>
    )

    return (
        <Sider
            width={330}
            collapsible={false}
            breakpoint={'md'}
            className={`${styles.menu} ${styles.light}`}>
            <div className={styles.orderNumber}>
                <div className={styles.orderNumberContainer}>
                    <p>{`Order Number: ${orderId || lastOrderN + 1}`}</p>
                </div>
            </div>
            <Scrollbars className={styles.scrollbarDesktop}>
                {
                    !!basket.length && (
                        <>
                            {/*TODO*/}
                            {/*style={{ maxHeight: window.innerHeight, overflowY: 'scroll' }}*/}
                            {/* CREATE SCROLL WHEN ITEMS LENGTH IS BIG THEN window.innerHeight */}
                            <div className="col-12">
                                <Basket basket={basket}/>
                            </div>
                            <div className="col-12 pb-4 mb-4 border-bottom">
                                <TotalAmount totalAmount={totalPrice}/>
                            </div>
                            <div className="col-12">
                                <Button
                                    onClick={
                                        orderId
                                            ? () => confirmationDialog(
                                            () => updateOrder(),
                                            attentionText,
                                            true,
                                            'After Order Update'
                                            )
                                            : createOrder
                                    }
                                    className={styles.finishAndPayButton}
                                    type="primary">
                                    {orderId ? 'Update Order' : 'Finish & Pay'}
                                </Button>
                            </div>
                        </>
                    )
                }
            </Scrollbars>
        </Sider>
    )
}

export default memo(OrderPreview)
