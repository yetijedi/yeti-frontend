import { Button } from 'antd'
import { PlusCircleOutlined, MinusCircleOutlined, DeleteOutlined } from '@ant-design/icons'
import React, { memo, useCallback } from 'react'
import styles from './style.module.scss'
import { useDispatch } from 'react-redux'
import { thousandsFormatter } from '../../../../../../services/formatters'
import { DEFAULT_CURRENCY } from '../../../../../../constants'

const BasketItem: React.FC<IProps> = ({ basketItem, setBasketItem }) => {
    const dispatch = useDispatch()

    const handlePlusQuantity = useCallback(() => {
        let q = basketItem.quantity
        q++
        dispatch(setBasketItem({ ...basketItem, quantity: q }))
    }, [basketItem])

    const handleMinusQuantity = useCallback(() => {
        let q = basketItem.quantity
        q--

        if (q < 1) {
            return
        }
        dispatch(setBasketItem({ ...basketItem, quantity: q }))
    }, [basketItem])

    const handleRemoveItemFromBasket = basketItem => {
        dispatch(setBasketItem({ ...basketItem, quantity: 0 }))
    };

    return (
        <div className={`${styles.basketItem} d-flex justify-content-between align-items-center pt-2 pb-2`}>
            <div className={`${styles.name || ''} pr-1`}>{basketItem.name || ''}</div>
            <div className={`${styles.quantity || ''} d-flex align-items-center justify-content-between pl-1 pr-1`}>
                <span>
                    <Button
                        onClick={handleMinusQuantity}
                        type="link"
                        size="small"
                        disabled={basketItem.quantity === 1}
                        icon={<MinusCircleOutlined/>}/>
                </span>
                <span className="pl-2 pr-2">{basketItem.quantity}</span>
                <span>
                    <Button
                        onClick={handlePlusQuantity}
                        type="link"
                        size="small"
                        icon={<PlusCircleOutlined/>}/>
                </span>
            </div>
            <div className={`${styles.price || ''} text-right pl-1 pr-1`}>
                {`${basketItem.itemTotalPrice ? thousandsFormatter(basketItem.itemTotalPrice) : 0} ${DEFAULT_CURRENCY}`}
            </div>
            <div className={`${styles.remove} justify-content-end d-flex pl-1`}>
                <Button
                    onClick={() => handleRemoveItemFromBasket(basketItem)}
                    type="link"
                    danger={true}
                    size="small"
                    icon={<DeleteOutlined />}/>
            </div>
        </div>
    )
}

interface IProps {
    basketItem: any,
    setBasketItem(basketItem)
}

export default memo(BasketItem)
