import React, { memo } from 'react'

import { workspaceOp } from '../../../../../store/ducks/workspace'
import BasketItem from './BasketItem'

interface IProps {

}

const Basket: React.FC<any> = ({ basket }) => {
    return (
        <div className="pt-2 pb-2 border-bottom mb-4">
            {Object.values(basket).map((item: any) => {
                return (
                    <BasketItem
                        setBasketItem={workspaceOp.setBasket}
                        basketItem={item}
                        key={item.id}
                    />
                )
            })}
        </div>
    )
}

export default memo(Basket)
