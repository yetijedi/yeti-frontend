import React, { memo, useCallback, useEffect } from 'react'
import { useHistory, useParams } from 'react-router'
import { Button, Form, Input, InputNumber, Select } from 'antd'
import { Link } from 'react-router-dom'
import { notify } from '../../../../services/notifier'
import { usersOp } from '../../../../store/ducks/users'
import ShopSelect from '../../../components/common/ShopSelect'

const { Option } = Select
const FormItem = Form.Item

const validateMessages = {
    // tslint:disable-next-line:no-invalid-template-strings
    required: '${label} is required!',
    types: {
        // tslint:disable-next-line:no-invalid-template-strings
        email: '${label} is not validate email!',
        // tslint:disable-next-line:no-invalid-template-strings
    },
}

function CreateUpdateUser() {
    const [form] = Form.useForm()
    const { id: userId } = useParams()
    const history = useHistory()

    const fetchSingleUser = async id => {
        try {
            const {
                data: {
                    first_name,
                    last_name,
                    phone,
                    email,
                    password,
                    role_id,
                    shop_id,
                    salary,
                    salary_type,
                    // is_active,
                },
            } = await usersOp.getSingleUserAsync(id)

            form.setFieldsValue({
                first_name,
                last_name,
                phone,
                email,
                password,
                role_id,
                shop_id,
                salary,
                salary_type,
                // is_active,
            })
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        if (userId) {
            fetchSingleUser(userId)
        }
    }, [])

    const handleCreateUpdateUser = useCallback(async e => {
        e.preventDefault()

        try {
            const validFields = await form.validateFields()
            if (userId) {
                await usersOp.updateUserAsync({
                    ...validFields,
                    // last_login: new Date(),
                    // tslint:disable-next-line:radix
                    id: parseInt(userId),
                })
                notify('User updated successfully')
            } else {
                await usersOp.createUserAsync({
                    ...validFields,
                    // last_login: new Date(),
                })
                notify('User created successfully')
            }

            history.push('/users')
        } catch (err) {
            // console.log(err)
            notify('Something went wrong!', 'error')
        }
    }, [])

    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select
                defaultValue="+374"
                style={{ width: 100 }}>
                <Option value="+374">+374</Option>
            </Select>
        </Form.Item>
    )

    return (
        <div>
            <div className="card">
                <div className="card-header">
                    <div className="utils__title">
                        <strong>{userId ? 'User Update' : 'User Create'}</strong>
                    </div>
                </div>
                <div className="card-body">
                    <Form validateMessages={validateMessages} form={form} layout="vertical">
                        <div className="row">
                            <div className="col-lg-8">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true }]}
                                                name="first_name"
                                                label="First name">
                                                <Input placeholder="First name"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true }]}
                                                name="last_name"
                                                label="Last name">
                                                <Input placeholder="Last name"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            {/* TODO  */}
                                            {/* PHONE VALIDATION */}
                                            <FormItem
                                                rules={[{ message: 'Please input your phone number!' }]}
                                                name="phone"
                                                label="Phone number">
                                                <Input
                                                    addonBefore={prefixSelector}
                                                    style={{ width: '100%' }}
                                                    placeholder="Phone"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ type: 'email' }]}
                                                name="email"
                                                label="Email">
                                                <Input
                                                    placeholder="Email"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ required: true }]}
                                                hasFeedback
                                                name="password"
                                                label="Password">
                                                <Input.Password/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <FormItem
                                                        rules={[{ required: true }]}
                                                        name="role_id"
                                                        label="Role">
                                                        <Select
                                                            showSearch={true}
                                                            placeholder="Select a Role"
                                                        >
                                                            <Option key="admin" value={1}>Admin</Option>
                                                            <Option key="terminal_user" value={2}>Terminal User</Option>
                                                        </Select>
                                                    </FormItem>
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <ShopSelect/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                rules={[{ type: 'number' }]}
                                                name="salary"
                                                label="Salary">
                                                <InputNumber
                                                    className="w-100"
                                                    placeholder="Salary"/>
                                            </FormItem>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="form-group">
                                            <FormItem
                                                name="salary_type"
                                                label="Salary Type">
                                                <Select
                                                    showSearch={true}
                                                    placeholder="Select a salary type"
                                                >
                                                    <Option key="percent" value="percent">Percent</Option>
                                                    <Option key="fixed" value="fixed">Fixed</Option>
                                                </Select>
                                            </FormItem>
                                        </div>
                                    </div>
                                    {/*<div className="col-lg-6">*/}
                                    {/*    <div className="form-group">*/}
                                    {/*        <FormItem*/}
                                    {/*            valuePropName="checked"*/}
                                    {/*            name="is_active"*/}
                                    {/*            label="Active user">*/}
                                    {/*            <Switch/>*/}
                                    {/*        </FormItem>*/}
                                    {/*    </div>*/}
                                    {/*</div>*/}
                                    <div className="col-lg-12">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-actions">
                                                    <Button
                                                        onClick={handleCreateUpdateUser}
                                                        type="primary"
                                                        className="mr-2">
                                                        {userId ? 'Update User' : 'Save User'}
                                                    </Button>
                                                    <Button type="default">
                                                        <Link to="/categories">
                                                            Cancel
                                                        </Link>
                                                    </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    )
}

export default memo(CreateUpdateUser)
