import { Button, Table } from 'antd'
import { DeleteOutlined, EditOutlined } from '@ant-design/icons'
import React, { memo, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { usersOp, usersSel } from '../../../store/ducks/users'
import { Link } from 'react-router-dom'
import { notify } from '../../../services/notifier'
import { dateFormatter } from '../../../services/formatters'
import { globalSel } from '../../../store/ducks/global'
import { METHODS, RESOURCE_CODES } from '../../../constants'
import { checkUserAccess } from '../../../services/checkUserAccess'
import { confirmationDialog } from '../../components/common/ConfirmationDialog'

function UsersList() {
    const dispatch = useDispatch()
    const { users } = useSelector(usersSel.usersSelector)
    const currentUser = useSelector(globalSel.currentUserSelector)
    const commonSettings = useSelector(globalSel.commonSettingsSelector)
    const { put: PUT, delete: DELETE } = METHODS
    const { users: USER } = RESOURCE_CODES

    const disabledActions = method => checkUserAccess(currentUser, commonSettings, USER, method)

    const columns = useMemo(() => ([
        {
            title: 'First Name',
            dataIndex: 'first_name',
            key: 'first_name',
        },
        {
            title: 'Last Name',
            dataIndex: 'last_name',
            key: 'last_name',
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Last Login',
            dataIndex: 'last_login',
            key: 'last_login',
            render: date => dateFormatter(date),
        },
        {
            title: 'Action',
            key: 'action',
            render: user => {
                return (
                    <>
                        <Button
                            disabled={!disabledActions(PUT)}
                            type="link">
                            <Link to={`/users/update/${user.id}`}>
                                <EditOutlined/>
                            </Link>
                        </Button>
                        <Button
                            disabled={!disabledActions(DELETE)}
                            onClick={
                                () => confirmationDialog(
                                    () => fetchDeleteUser(user.id), `${user.first_name} ${user.last_name}`,
                                )
                            }
                            type="link" danger={true}>
                            <DeleteOutlined/>
                        </Button>
                    </>
                )
            },
        },
    ]), [])

    const fetchDeleteUser = async userId => {
        try {
            // await usersSel.deleteUserAsync(userId)
            await dispatch(usersOp.getUsersAsync())
            notify('User deleted successfully!')
        } catch (e) {
            // console.log(e)
            notify('Something went wrong!', 'error')
        }
    }

    useEffect(() => {
        dispatch(usersOp.getUsersAsync())
    }, [])

    return (
        <Table
            dataSource={users}
            columns={columns}
        />
    )
}

export default memo(UsersList)
