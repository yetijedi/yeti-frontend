import React from 'react'
import { Progress } from 'antd'
import { progressGroup } from './data.json';
class ProgressGroup extends React.Component<any> {
  render() {
    return (
      <div>
        <strong>{progressGroup.first.name}</strong>
        <p className="text-muted mb-1">{progressGroup.first.description}</p>
        <div className="mb-3">
          <Progress percent={progressGroup.first.progress} status={'active'} />
        </div>
      </div>
    )
  }
}
export default ProgressGroup
