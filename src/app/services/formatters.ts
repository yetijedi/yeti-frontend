import moment from 'moment'

/**
 *  Date Formatter
 *  @param date {Date}
 */
export const dateFormatter = date => moment(date).format('DD MMM YYYY, HH:mm')

/**
 * Thousands Formatter
 * @param n {number}
 */
export const thousandsFormatter = n => Number(n).toLocaleString()
