import { message } from 'antd'

/**
 *  Notifier
 *  @param msg {string}
 *  @param type {string}
 */
export const notify = (msg, type = 'success') => {
    message[type](msg)
}
