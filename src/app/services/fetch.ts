import axios, { AxiosError, AxiosPromise, AxiosRequestConfig, Method } from 'axios';
import { getStore } from "../store/configureStore";

import { logError } from "./errorLogger";
import { API_ENDPOINT } from "../constants";
import { globalOp } from "../store/ducks/global";

interface IFetchOptions {
    url?: string;
    endPoint: string;
    method?: Method;
    body?: any;
    headers?: any
}

class ErrorResponse {
    response: Object;
    constructor(err) {
        this.response = {
            data: err
        }
    }
}

/**
 * @private
 */
const getDefaultHeaders = () => ({
    "Accept": "application/json",
    'Content-Type': "application/json"
});

/**
 * @private
 * Contain all pending requests Promises
 */
const _pendingRequests = [];

/**
 * @private
 * Axios error response interceptor
 */
const responseInterceptorOnError = (error: AxiosError) => {
    if (axios.isCancel(error)) {
        return Promise.reject(new ErrorResponse(error))
    }
    if (error.response) {
        if (401 === error.response.status) {
            getStore().dispatch(globalOp.logoutAsync());
            return Promise.reject(error);
        } else {
            // Store the logs and ignore the promise
            logError(error);
            return Promise.reject(error);
        }
    }
    return Promise.reject(new ErrorResponse(error));
};

// Add a response interceptor
axios.interceptors.response.use(
    response => response,
    responseInterceptorOnError
);

/**
 * @public
 * @param url
 * @param endPoint
 * @param body
 * @param method
 * @param headers
 */
const fetch = ({
   url = API_ENDPOINT,
   endPoint = '/',
   body,
   method = 'GET',
   headers = {}
}: IFetchOptions): AxiosPromise => {
    const curToken = axios.CancelToken.source();
    _pendingRequests.push(curToken);

    let config: AxiosRequestConfig = {
        withCredentials: true,
        method,
        url: `${url}${endPoint}`,
        cancelToken: curToken.token,
        data: body
    };

    config.headers = {
        ...getDefaultHeaders(),
        ...headers
    };

    if (method === 'GET') {
        config.params = body;
    }

    return axios(config);
};

/**
 * @public
 */
const abortPendingRequest = (): boolean => {
    if (_pendingRequests.length) {
        _pendingRequests.map(req => req.cancel('HTTP Request aborted'));
        return true;
    }
    return false;
};

export {
    fetch as default,
    abortPendingRequest
}
