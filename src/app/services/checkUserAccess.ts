/**
 * ACCESS CHECKING
 * @param currentUser {object}
 * @param commonSettings {object}
 * @param resource {string}
 * @param method {string}
 */
export function checkUserAccess(currentUser: any, commonSettings: any, resource: string = '', method: string = '') {
    if (currentUser && commonSettings) {
        const { permissions } = currentUser;
        const { resourceCodes } = commonSettings;

        if (!Object.keys(permissions).length) return false;

        if (!permissions[resourceCodes[resource]] || !(resourceCodes[resource] in permissions) ) return false;

        if (typeof permissions[resourceCodes[resource]] === 'boolean') return true;

        return permissions[resourceCodes[resource]].includes(method);
    }

    return null;
}
